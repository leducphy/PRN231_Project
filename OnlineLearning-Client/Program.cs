using Microsoft.AspNetCore.Authentication.Cookies;

namespace OnlineLearning_Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllersWithViews();
            builder.Services.AddAuthentication("CookieAuthentication")
                .AddCookie("CookieAuthentication", options =>
                {
                    options.LoginPath = "/Login";
                    options.AccessDeniedPath = "/Error/Forbidden";
                    options.ExpireTimeSpan = TimeSpan.FromHours(23).Add(TimeSpan.FromMinutes(50));
                });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "CustomRoutePost",
                    pattern: "Class/{classCode}/Post",
                    defaults: new { controller = "Class", action = "Post" }
                );
                endpoints.MapControllerRoute(
                    name: "CustomRouteCurriculum",
                    pattern: "Class/{classId}/Curriculum",
                    defaults: new { controller = "Class", action = "Curriculum" }
                );
                endpoints.MapControllerRoute(
                    name: "CustomRouteCreateCurriculum",
                    pattern: "Class/{classId}/Curriculum/Create",
                    defaults: new { controller = "Class", action = "CreateCurriculum" }
                );
            });

            app.Run();
        }
    }
}