﻿

$(document).ready(function () {
    $(window).scroll(function () {
        var currentScroll = $(window).scrollTop();
        if (currentScroll >= 100) {
            $("header").addClass("active");
        } else {
            $("header").removeClass("active");
        }
    });

    $(".btn-signin").click(function () {
        $(".form-signin").addClass("active");
        $(".form-signup").removeClass("active");
        $(".form-reset").removeClass("active");
        $("body").addClass("no-scroll");
        $(".overlay").addClass("show1");
    });

    $(".btn-signup").click(function () {
        $(".form-signup").addClass("active");
        $(".form-signin").removeClass("active");
        $(".form-reset").removeClass("active");
        $("body").addClass("no-scroll");
        $(".overlay").addClass("show1");
    });

    $(".btn-reset").click(function () {
        $(".form-reset").addClass("active");
        $(".form-signin").removeClass("active");
        $(".form-signup").removeClass("active");
        $("body").addClass("no-scroll");
        $(".overlay").addClass("show1");
    });

    $(".btn-close-form").click(function () {
        $(".form-reset").removeClass("active");
        $(".form-signin").removeClass("active");
        $(".form-signup").removeClass("active");
        $("body").removeClass("no-scroll");
        $(".overlay").removeClass("show1");
    });
})

const navLinks = document.querySelectorAll('.nav-link');

// Add a click event listener to each nav-link element
navLinks.forEach(link => {
    link.addEventListener('click', function () {
        // Remove the active-navbar class from all divs
        document.querySelectorAll('.navbar-item').forEach(div => {
            div.classList.remove('active-navbar');
        });

        // Add the active-navbar class to the parent div of the clicked link
        this.parentNode.classList.add('active-navbar');
    });
});

// script show profile on header
var headerProfileAvatar = document.getElementById("avatarWrapper");
var headerProfileDropdownArrow = document.getElementById("dropdownWrapperArrow");
var headerProfileDropdown = document.getElementById("dropdownWrapper");

document.addEventListener("click", function (event) {
    var headerProfileDropdownClickedWithin = headerProfileDropdown.contains(event.target);

    if (!headerProfileDropdownClickedWithin) {
        if (headerProfileDropdown.classList.contains("active")) {
            headerProfileDropdown.classList.remove("active");
            headerProfileDropdownArrow.classList.remove("active");
        }
    }
});

headerProfileAvatar.addEventListener("click", function (event) {
    headerProfileDropdown.classList.toggle("active");
    headerProfileDropdownArrow.classList.toggle("active");
    event.stopPropagation();
});

function closeTextHi() {
    var textAI = document.getElementById('text-ai-over');
    textAI.style.display = 'none';
}

function openChatBoxAI() {
    var chatboxAI = document.getElementById('chatbox-AI-layout');
    chatboxAI.style.display = 'block';
}

function closeChatboxAI() {
    var chatboxAI = document.getElementById('chatbox-AI-layout');
    chatboxAI.style.display = 'none';
}

const textarea = document.getElementById("ai-textarea");
const placeholder = document.getElementById("ai-placeholder");

textarea.addEventListener("input", function () {
    placeholder.style.display = this.value ? "none" : "block";
});

const aiText = document.querySelector(".ai-text");
aiText.addEventListener("click", function () {
    textarea.focus();
});
