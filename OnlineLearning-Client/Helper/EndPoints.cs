﻿namespace OnlineLearning_Client.Helper
{
    public static class EndPoints
    {
        
        private static string BuildUrl(string path)
        {
            return path;
        }

        // Account Endpoints
        public static string SignIn => BuildUrl("Account/SignIn");
        public static string SignUp => BuildUrl("Account/SignUp");
      
    }
}
