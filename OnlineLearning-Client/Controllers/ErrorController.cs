﻿using Microsoft.AspNetCore.Mvc;

namespace OnlineLearning_Client.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Forbidden()
        {
            return View();
        }
    }
}
