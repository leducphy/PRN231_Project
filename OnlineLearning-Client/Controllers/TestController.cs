using BusinessObject.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace OnlineLearning_Client.Controllers
{
    public class TestController : Controller
    {
        public IActionResult Transcript()
        {
            ViewBag.ShowSidebar = true;
            ViewBag.ActiveSidebar = "Transcript";
            return View();
        }

        [Authorize(Roles = RoleConstant.TEACHER)]
        public IActionResult Create()
        {
            ViewBag.ShowSidebar = true;
            ViewBag.ActiveSidebar = "Exercise";
            return View();
        }

        [Authorize(Roles = RoleConstant.TEACHER)]
        public IActionResult List()
        {
            ViewBag.ShowSidebar = true;
            ViewBag.ActiveSidebar = "Exercise";
            return View();
        }
    }
}