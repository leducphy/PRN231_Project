﻿using BusinessObject.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.FormulaParsing.LexicalAnalysis;
using OnlineLearning_Client.Services;

namespace OnlineLearning_Client.Controllers
{
    public class AdminController : Controller
    {
        [Authorize(Roles = RoleConstant.ADMIN)]
        public async Task<IActionResult> Dashboard()
        {
            return View();
        }
    }
}
