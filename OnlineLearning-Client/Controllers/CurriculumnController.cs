﻿// using Microsoft.AspNetCore.Mvc;
// using OnlineLearning_Client.Models;
// using OnlineLearning_Client.Services;
//
// namespace OnlineLearning_Client.Controllers
// {
//     public class CurriculumnController : Controller
//     {
//         public async Task<IActionResult> Index(Guid classId)
//         {
//             ViewBag.ActiveSidebar = "Curriculum";
//             ViewBag.ShowSidebar = true;
//             var id = classId;
//             var client = new ClientService(HttpContext);
//             var curriculumResponse = await client.GetDetail<CurriculumnResponse>("/api/Curriculum/GetByID", $"?id={id}");
//             return View("Class/Curriculum/Index",curriculumResponse);
//         }
//
//         public IActionResult Create()
//         {
//             ViewBag.ShowSidebar = true;
//             return View();
//         }
//     }
// }

