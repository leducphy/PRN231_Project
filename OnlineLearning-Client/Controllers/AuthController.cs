﻿using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using OnlineLearning_Client.Models;
using Microsoft.AspNetCore.Authentication;
using OnlineLearning_Client.Helper;
using System.Text;
using Newtonsoft.Json;
using OnlineLearning_Client.Services;
using System.Security.Claims;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using BusinessObject.Utility;
using Microsoft.AspNetCore.Authorization;

namespace OnlineLearning_Client.Controllers
{
    public class AuthController : Controller
    {
        private readonly HttpClient _client;

        [HttpGet]
        public IActionResult ResetPassword()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Verify()
        {
            return View();
        }


        [HttpGet]
        public IActionResult LogOut()
        {
            HttpContext.SignOutAsync("CookieAuthentication");
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> SignIn(LoginRequest loginRequest)
        {
            try
            {
                var client = new ClientService(HttpContext);
                var requestModel = new LoginRequest { Email = loginRequest.Email, Password = loginRequest.Password };
                var result = await client.PostReturnResponse("/api/Auth/SignIn", requestModel);

                if (result.IsSuccessStatusCode)
                {
                    var content = await result.Content.ReadAsStringAsync();
                    var res = JsonConvert.DeserializeObject<LoginResponseModel>(content);

                    // Set the authentication cookie
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Sid, res.Id.ToString()),
                        new Claim(ClaimTypes.Name, res.FullName),
                        new Claim(ClaimTypes.Email, res.Email),
                        new Claim(ClaimTypes.Role, res.Role)
                    };
                    var claimsIdentity = new ClaimsIdentity(claims, "login");
                    var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                    HttpContext.Response.Cookies.Append("AccessToken", res.Token, new CookieOptions
                    {
                        Expires = DateTime.Now.AddDays(1).AddMinutes(-1)
                    });


                    await HttpContext.SignInAsync("CookieAuthentication", claimsPrincipal,
                        new AuthenticationProperties { IsPersistent = loginRequest.KeepLoggedIn });
                }
                else if (result.StatusCode == HttpStatusCode.BadRequest)
                {
                    TempData["NotificationType"] = "error";
                    TempData["Message"] = "Đăng nhập thất bại, vui lòng thử lại ..";
                    TempData["ShowLoginForm"] = true;
                    TempData["ShowSignupForm"] = false;
                    return RedirectToAction("Index", "Home");
                }

                //TempData["NotificationType"] = "error";
                //TempData["Message"] = "An unexpected error occurred during login.";
            }
            catch (Exception ex)
            {
                // Log the exception for debugging purposes
                //TempData["NotificationType"] = "error";
                //TempData["Message"] = $"An error occurred during login {ex}.";
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> SignUp(SignUpRequest signUpRequest)
        {
            var client = new ClientService(HttpContext);
            var requestModel = new SignUpRequest
            {
                Email = signUpRequest.Email,
                Password = signUpRequest.Password,
                Name = signUpRequest.Name,
                ProfilePicture = "",
                Role = signUpRequest.Role,
            };

            var result = await client.PostReturnResponse("/api/Auth/SignUp", requestModel);

            if (result.IsSuccessStatusCode)
            {
                // SignUp was successful, redirect to SignIn page

                TempData["NotificationType"] = "success";
                TempData["Message"] = "Vui lòng kiểm tra Email của bạn để xác thực tài khoản...";
                TempData["ShowLoginForm"] = true;
                TempData["ShowSignupForm"] = false;
            }
            else if (result.StatusCode == HttpStatusCode.BadRequest)
            {
                // SignUp failed
                TempData["NotificationType"] = "error";
                TempData["Message"] = "Email đã tồn tại trong hệ thống ";
                TempData["ShowLoginForm"] = false;
                TempData["ShowSignupForm"] = true;
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet("ResetPassword")]
        public async Task<IActionResult> ResetPassword(Guid token)
        {
            try
            {
                var client = new ClientService(HttpContext);
                var result = await client.PostReturnResponse("/api/Auth/ResetPassword", token);
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                return View();
            }
        }


        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordRequest rq)
        {
            try
            {
                var client = new ClientService(HttpContext);
                var result = await client.PostReturnResponse("/api/Auth/ResetPassword", rq);
                var content = result.Content.ReadAsStringAsync().Result;
                if (result.IsSuccessStatusCode)
                {
                    TempData["NotificationType"] = "success";
                    TempData["Message"] = "Đổi mật khẩu thành công...";
                }
                else if (result.StatusCode == HttpStatusCode.BadRequest)
                {
                    TempData["NotificationType"] = "error";
                    TempData["Message"] = content;
                }

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordRequest forgorModel)
        {
            try
            {
                var client = new ClientService(HttpContext);
                var result = await client.PostReturnResponse("/api/Auth/ForgotPassword", forgorModel);

                if (result.IsSuccessStatusCode)
                {
                    // Forgot password request was successful
                    TempData["NotificationType"] = "success";
                    TempData["Message"] = "Vui lòng kiểm tra Email của bạn để đổi mật khẩu mới...";
                    TempData["ShowLoginForm"] = true;
                    TempData["ShowSignupForm"] = false;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    // Forgot password request failed
                    TempData["NotificationType"] = "error";
                    TempData["Message"] = "Có gì đó không đúng vui lòng kiểm tra lại...";
                    TempData["ShowLoginForm"] = true;
                    TempData["ShowSignupForm"] = false;
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                // Log the exception for debugging purposes
                TempData["NotificationType"] = "error";
                TempData["Message"] = $"Có gì đó không đúng vui lòng kiểm tra lại...{ex}";
                TempData["ShowLoginForm"] = false;
                TempData["ShowSignupForm"] = false;
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = RoleConstant.TEACHER + "," + RoleConstant.STUDENT)]
        public IActionResult MyProfile()
        {
            return View();
        }
    }
}