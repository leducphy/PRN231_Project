﻿using BusinessObject.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using OnlineLearning_Client.Models;
using OnlineLearning_Client.Services;

namespace OnlineLearning_Client.Controllers
{
    public class ClassController : Controller
    {
        [Authorize(Roles = RoleConstant.STUDENT)]
        public IActionResult JoinClass()
        {
            ViewBag.ShowSidebar = false;
            return View();
        }

        public IActionResult Post()
        {
            ViewBag.ShowSidebar = true;
            return View();
        }


        [Authorize(Roles = RoleConstant.TEACHER)]
        public IActionResult ListClass()
        {
            ViewBag.ActiveSidebar = "Home";
            return View();
        }

        [Authorize(Roles = RoleConstant.STUDENT)]
        public IActionResult ListClassStudent()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var accountIdClaim = claimsIdentity?.FindFirst(ClaimTypes.Sid);
            ViewBag.AccountID = accountIdClaim;
            return View();
        }

        public IActionResult MembersInClass()
        {
            ViewBag.ShowSidebar = true;
            return View();
        }

        public IActionResult MembersApprove()
        {
            ViewBag.ShowSidebar = true;
            return View();
        }

        public IActionResult CreateClass()
        {
            return View();
        }

        public IActionResult ClassSettings()
        {
            return View();
        }

        public async Task<IActionResult> Curriculum(Guid classId)
        {
            ViewBag.ActiveSidebar = "Curriculum";
            ViewBag.ShowSidebar = true;
            var client = new ClientService(HttpContext);
            var curriculumResponse =
                await client.GetDetail<CurriculumResponse>("/api/Curriculum/GetByID", $"?id={classId}");
            return View("Curriculum/Index", curriculumResponse);
        }

        public IActionResult CreateCurriculum(Guid classId)
        {
            ViewBag.ShowSidebar = true;
            ViewBag.ClassID = classId;
            return View("Curriculum/Create");
        }
    }
}