﻿namespace OnlineLearning_Client.Models
{
    public class ResetPasswordRequest
    {
        public Guid token { get; set; }
        public string password { get; set; } = string.Empty;
        public string confirmPassword { get; set; } = string.Empty;
    }

    public class ForgotPasswordRequest
    {
        public string Email { get; set; } = string.Empty;
    }
}