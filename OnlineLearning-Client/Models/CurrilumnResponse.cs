﻿namespace OnlineLearning_Client.Models
{
    public class CurriculumResponse
    {
        public Guid CurriculumId { get; set; }
        public string CurriculumTitle { get; set; } = string.Empty;
        public Guid ClassId { get; set; }
        public List<ChapterResponse> Chapters { get; set; }
    }

    public class ChapterResponse
    {
        public Guid ChapterId { get; set; }
        public string ChapterTitle { get; set; } = string.Empty;
        public List<MaterialResponse> Materials { get; set; }
    }

    public class MaterialResponse
    {
        public Guid MaterialId { get; set; }
        public Guid ResourceId { get; set; }
        public string MaterialTitle { get; set; }
    }
}