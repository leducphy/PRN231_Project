﻿namespace OnlineLearning_Client.Models
{
    public class SignUpRequest
    {
        public string Name { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Password { get; set; } = null!;
        public byte Role { get; set; }
        public string? ProfilePicture { get; set; }

    }
}
