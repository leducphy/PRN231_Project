﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Class
    {
        public Class()
        {
            Curricula = new HashSet<Curriculum>();
            Enrollments = new HashSet<Enrollment>();
            Notifications = new HashSet<Notification>();
            Posts = new HashSet<Post>();
            Tests = new HashSet<Test>();
        }

        public Guid ClassId { get; set; }
        public Guid AccountId { get; set; }
        public string Name { get; set; } = null!;
        public string Code { get; set; } = null!;
        public bool? EnrollApprove { get; set; }
        public string? ClassPicture { get; set; }
        public bool? Hidden { get; set; }
        public DateTime? CreateTime { get; set; }
        public string? ClassDescription { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual ICollection<Curriculum> Curricula { get; set; }
        public virtual ICollection<Enrollment> Enrollments { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Test> Tests { get; set; }
    }
}
