﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Post
    {
        public Post()
        {
            Comments = new HashSet<Comment>();
            Resources = new HashSet<Resource>();
        }

        public Guid PostId { get; set; }
        public Guid AccountId { get; set; }
        public Guid ClassId { get; set; }
        public bool? Pin { get; set; }
        public string Content { get; set; } = null!;
        public DateTime? CreateTime { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Resource> Resources { get; set; }
    }
}
