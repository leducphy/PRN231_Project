﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class StudentAnswer
    {
        public Guid AccountId { get; set; }
        public Guid QuestionId { get; set; }
        public Guid AnswerId { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual Answer Answer { get; set; } = null!;
        public virtual Question Question { get; set; } = null!;
    }
}
