﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Enrollment
    {
        public Guid AccountId { get; set; }
        public Guid ClassId { get; set; }
        public bool Accepted { get; set; }
        public DateTime? EnrollTime { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual Class Class { get; set; } = null!;
    }
}
