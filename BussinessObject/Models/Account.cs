﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Account
    {
        public Account()
        {
            Classes = new HashSet<Class>();
            Comments = new HashSet<Comment>();
            DoTests = new HashSet<DoTest>();
            Enrollments = new HashSet<Enrollment>();
            Feedbacks = new HashSet<Feedback>();
            NotificationAccounts = new HashSet<Notification>();
            NotificationTargetNavigations = new HashSet<Notification>();
            Posts = new HashSet<Post>();
            Resources = new HashSet<Resource>();
            StudentAnswers = new HashSet<StudentAnswer>();
        }

        public Guid AccountId { get; set; }
        public string Name { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Password { get; set; } = null!;
        public byte Role { get; set; }
        public string? ProfilePicture { get; set; }
        public DateTime? VerifiedAt { get; set; }
        public Guid? VerificationCode { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? PasswordResetTokenExpires { get; set; }
        public Guid? PasswordResetToken { get; set; }
        public bool? Locked { get; set; }
        public DateTime? Dob { get; set; }
        public bool? Gender { get; set; }

        public string? Phone { get; set; }

        public string? Address { get; set; }

        public string? School { get; set; }



        public virtual ICollection<Class> Classes { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<DoTest> DoTests { get; set; }
        public virtual ICollection<Enrollment> Enrollments { get; set; }
        public virtual ICollection<Feedback> Feedbacks { get; set; }
        public virtual ICollection<Notification> NotificationAccounts { get; set; }
        public virtual ICollection<Notification> NotificationTargetNavigations { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Resource> Resources { get; set; }
        public virtual ICollection<StudentAnswer> StudentAnswers { get; set; }
    }
}
