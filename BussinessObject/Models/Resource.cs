﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Resource
    {
        public Resource()
        {
            Answers = new HashSet<Answer>();
            Comments = new HashSet<Comment>();
            Materials = new HashSet<Material>();
            Questions = new HashSet<Question>();
            Tests = new HashSet<Test>();
        }

        public Guid ResourceId { get; set; }
        public Guid? PostId { get; set; }
        public Guid AccountId { get; set; }
        public string Url { get; set; } = null!;
        public string? Thumbnail { get; set; }
        public string MimeType { get; set; } = null!;
        public bool? Deleted { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual Post? Post { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Material> Materials { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Test> Tests { get; set; }
    }
}
