﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Comment
    {
        public Guid CommentId { get; set; }
        public Guid AccountId { get; set; }
        public Guid PostId { get; set; }
        public Guid? ResourceId { get; set; }
        public string Content { get; set; } = null!;
        public DateTime? CreateTime { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual Post Post { get; set; } = null!;
        public virtual Resource? Resource { get; set; }
    }
}
