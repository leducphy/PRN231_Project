﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Feedback
    {
        public Guid FeedbackId { get; set; }
        public Guid AccountId { get; set; }
        public string Title { get; set; } = null!;
        public string Content { get; set; } = null!;
        public string? Response { get; set; }
        public DateTime? CreateTime { get; set; }

        public virtual Account Account { get; set; } = null!;
    }
}
