﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Notification
    {
        public Guid NotificationId { get; set; }
        public Guid AccountId { get; set; }
        public Guid? ClassId { get; set; }
        public Guid? Target { get; set; }
        public byte? Type { get; set; }
        public string Title { get; set; } = null!;
        public string? RedirectUrl { get; set; }
        public string Content { get; set; } = null!;
        public DateTime? CreateTime { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual Class? Class { get; set; }
        public virtual Account? TargetNavigation { get; set; }
    }
}
