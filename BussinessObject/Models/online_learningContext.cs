﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace BussinessObject.Models
{
    public partial class online_learningContext : DbContext
    {
        public online_learningContext()
        {
        }

        public online_learningContext(DbContextOptions<online_learningContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; } = null!;
        public virtual DbSet<Answer> Answers { get; set; } = null!;
        public virtual DbSet<Chapter> Chapters { get; set; } = null!;
        public virtual DbSet<Class> Classes { get; set; } = null!;
        public virtual DbSet<Comment> Comments { get; set; } = null!;
        public virtual DbSet<Curriculum> Curricula { get; set; } = null!;
        public virtual DbSet<DoTest> DoTests { get; set; } = null!;
        public virtual DbSet<Enrollment> Enrollments { get; set; } = null!;
        public virtual DbSet<Feedback> Feedbacks { get; set; } = null!;
        public virtual DbSet<Material> Materials { get; set; } = null!;
        public virtual DbSet<Notification> Notifications { get; set; } = null!;
        public virtual DbSet<Post> Posts { get; set; } = null!;
        public virtual DbSet<Question> Questions { get; set; } = null!;
        public virtual DbSet<Resource> Resources { get; set; } = null!;
        public virtual DbSet<StudentAnswer> StudentAnswers { get; set; } = null!;
        public virtual DbSet<Test> Tests { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var ConnectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetConnectionString("DefaultConnection");
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("account");

                entity.HasIndex(e => e.Email, "UQ__account__AB6E61647DEED14A")
                    .IsUnique();

                entity.Property(e => e.AccountId)
                    .HasColumnName("account_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address")
                    .IsFixedLength();

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("dob");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.Gender).HasColumnName("gender");

                entity.Property(e => e.Locked)
                    .HasColumnName("locked")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .HasColumnName("name");

                entity.Property(e => e.Password)
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasColumnName("password")
                    .IsFixedLength();

                entity.Property(e => e.PasswordResetToken).HasColumnName("password_reset_token");

                entity.Property(e => e.PasswordResetTokenExpires)
                    .HasColumnType("datetime")
                    .HasColumnName("password_reset_token_expires");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("phone");

                entity.Property(e => e.ProfilePicture)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("profile_picture");

                entity.Property(e => e.Role).HasColumnName("role");

                entity.Property(e => e.School)
                    .HasMaxLength(255)
                    .HasColumnName("school")
                    .IsFixedLength();

                entity.Property(e => e.VerificationCode).HasColumnName("verification_code");

                entity.Property(e => e.VerifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("verified_at");
            });

            modelBuilder.Entity<Answer>(entity =>
            {
                entity.ToTable("answer");

                entity.Property(e => e.AnswerId)
                    .HasColumnName("answer_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AnswerOrder).HasColumnName("answer_order");

                entity.Property(e => e.Content)
                    .HasColumnType("text")
                    .HasColumnName("content");

                entity.Property(e => e.IsCorrect)
                    .HasColumnName("isCorrect")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.QuestionId).HasColumnName("question_id");

                entity.Property(e => e.ResourceId).HasColumnName("resource_id");

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.Answers)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__answer__question__6C190EBB");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Answers)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK__answer__resource__6D0D32F4");
            });

            modelBuilder.Entity<Chapter>(entity =>
            {
                entity.ToTable("chapter");

                entity.Property(e => e.ChapterId)
                    .HasColumnName("chapter_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ChapterTitle)
                    .HasMaxLength(255)
                    .HasColumnName("chapter_title")
                    .IsFixedLength();

                entity.Property(e => e.CurriculumId).HasColumnName("curriculum_id");

                entity.HasOne(d => d.Curriculum)
                    .WithMany(p => p.Chapters)
                    .HasForeignKey(d => d.CurriculumId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_chapter_curriculum");
            });

            modelBuilder.Entity<Class>(entity =>
            {
                entity.ToTable("class");

                entity.Property(e => e.ClassId)
                    .HasColumnName("class_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.ClassDescription).HasColumnName("class_description");

                entity.Property(e => e.ClassPicture)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("class_picture");

                entity.Property(e => e.Code)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("code")
                    .IsFixedLength();

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EnrollApprove)
                    .HasColumnName("enroll_approve")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Hidden)
                    .HasColumnName("hidden")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("name");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Classes)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__class__account_i__6E01572D");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.ToTable("comment");

                entity.Property(e => e.CommentId)
                    .HasColumnName("comment_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.Content)
                    .HasColumnType("text")
                    .HasColumnName("content");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PostId).HasColumnName("post_id");

                entity.Property(e => e.ResourceId).HasColumnName("resource_id");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__comment__account__6EF57B66");

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__comment__post_id__6FE99F9F");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK__comment__resourc__70DDC3D8");
            });

            modelBuilder.Entity<Curriculum>(entity =>
            {
                entity.ToTable("curriculum");

                entity.Property(e => e.CurriculumId)
                    .HasColumnName("curriculum_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ClassId).HasColumnName("class_id");

                entity.Property(e => e.CurriculumTitle)
                    .HasMaxLength(255)
                    .HasColumnName("curriculum_title");

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.Curricula)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_curriculum_class");
            });

            modelBuilder.Entity<DoTest>(entity =>
            {
                entity.HasKey(e => new { e.TestId, e.AccountId })
                    .HasName("PK__do_test__37953E2E9BD8478D");

                entity.ToTable("do_test");

                entity.Property(e => e.TestId).HasColumnName("test_id");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.FinishTime)
                    .HasColumnType("datetime")
                    .HasColumnName("finish_time");

                entity.Property(e => e.Score).HasColumnName("score");

                entity.Property(e => e.StartTime)
                    .HasColumnType("datetime")
                    .HasColumnName("start_time")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.DoTests)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__do_test__account__71D1E811");

                entity.HasOne(d => d.Test)
                    .WithMany(p => p.DoTests)
                    .HasForeignKey(d => d.TestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__do_test__test_id__72C60C4A");
            });

            modelBuilder.Entity<Enrollment>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.ClassId })
                    .HasName("PK__enrollme__397D6555502F0DA3");

                entity.ToTable("enrollment");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.ClassId).HasColumnName("class_id");

                entity.Property(e => e.Accepted).HasColumnName("accepted");

                entity.Property(e => e.EnrollTime)
                    .HasColumnType("datetime")
                    .HasColumnName("enroll_time");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Enrollments)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__enrollmen__accou__73BA3083");

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.Enrollments)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__enrollmen__class__74AE54BC");
            });

            modelBuilder.Entity<Feedback>(entity =>
            {
                entity.ToTable("feedback");

                entity.Property(e => e.FeedbackId)
                    .HasColumnName("feedback_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.Content)
                    .HasColumnType("text")
                    .HasColumnName("content");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Response)
                    .HasColumnType("text")
                    .HasColumnName("response");

                entity.Property(e => e.Title)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("title");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Feedbacks)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__feedback__accoun__75A278F5");
            });

            modelBuilder.Entity<Material>(entity =>
            {
                entity.ToTable("material");

                entity.Property(e => e.MaterialId)
                    .HasColumnName("material_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ChapterId).HasColumnName("chapter_id");

                entity.Property(e => e.MaterialTitle)
                    .HasMaxLength(255)
                    .HasColumnName("material_title")
                    .IsFixedLength();

                entity.Property(e => e.ResourceId).HasColumnName("resource_id");

                entity.HasOne(d => d.Chapter)
                    .WithMany(p => p.Materials)
                    .HasForeignKey(d => d.ChapterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_material_chapter");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Materials)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_material_resource");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.ToTable("notification");

                entity.Property(e => e.NotificationId)
                    .HasColumnName("notification_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.ClassId).HasColumnName("class_id");

                entity.Property(e => e.Content)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("content");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RedirectUrl)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("redirect_url");

                entity.Property(e => e.Target).HasColumnName("target");

                entity.Property(e => e.Title)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("title");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.NotificationAccounts)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__notificat__accou__76969D2E");

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.ClassId)
                    .HasConstraintName("FK__notificat__class__778AC167");

                entity.HasOne(d => d.TargetNavigation)
                    .WithMany(p => p.NotificationTargetNavigations)
                    .HasForeignKey(d => d.Target)
                    .HasConstraintName("FK__notificat__targe__787EE5A0");
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.ToTable("post");

                entity.Property(e => e.PostId)
                    .HasColumnName("post_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.ClassId).HasColumnName("class_id");

                entity.Property(e => e.Content)
                    .HasColumnType("text")
                    .HasColumnName("content");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Pin)
                    .HasColumnName("pin")
                    .HasDefaultValueSql("((0))");

                

               
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.ToTable("question");

                entity.Property(e => e.QuestionId)
                    .HasColumnName("question_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Content)
                    .HasColumnType("text")
                    .HasColumnName("content");

                entity.Property(e => e.QuestionOrder).HasColumnName("question_order");

                entity.Property(e => e.ResourceId).HasColumnName("resource_id");

                entity.Property(e => e.TestId).HasColumnName("test_id");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Questions)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK__question__resour__7D439ABD");

                entity.HasOne(d => d.Test)
                    .WithMany(p => p.Questions)
                    .HasForeignKey(d => d.TestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__question__test_i__7E37BEF6");
            });

            modelBuilder.Entity<Resource>(entity =>
            {
                entity.ToTable("resource");

                entity.Property(e => e.ResourceId)
                    .HasColumnName("resource_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MimeType)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("mime_type");

                entity.Property(e => e.PostId).HasColumnName("post_id");

                entity.Property(e => e.Thumbnail)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("thumbnail");

                entity.Property(e => e.Url)
                    .IsUnicode(false)
                    .HasColumnName("url");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Resources)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__resource__accoun__7F2BE32F");

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.Resources)
                    .HasForeignKey(d => d.PostId)
                    .HasConstraintName("FK_resource_post");
            });

            modelBuilder.Entity<StudentAnswer>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.QuestionId, e.AnswerId })
                    .HasName("PK__student___DD7D71DA750CD966");

                entity.ToTable("student_answer");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.QuestionId).HasColumnName("question_id");

                entity.Property(e => e.AnswerId).HasColumnName("answer_id");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.StudentAnswers)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__student_a__accou__00200768");

                entity.HasOne(d => d.Answer)
                    .WithMany(p => p.StudentAnswers)
                    .HasForeignKey(d => d.AnswerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__student_a__answe__01142BA1");

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.StudentAnswers)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__student_a__quest__02084FDA");
            });

            modelBuilder.Entity<Test>(entity =>
            {
                entity.ToTable("test");

                entity.Property(e => e.TestId)
                    .HasColumnName("test_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AllowReview)
                    .HasColumnName("allow_review")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ClassId).HasColumnName("class_id");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .HasColumnType("text")
                    .HasColumnName("description");

                entity.Property(e => e.Duration).HasColumnName("duration");

                entity.Property(e => e.EndAt)
                    .HasColumnType("datetime")
                    .HasColumnName("end_at");

                entity.Property(e => e.ResourceId).HasColumnName("resource_id");

                entity.Property(e => e.StartAt)
                    .HasColumnType("datetime")
                    .HasColumnName("start_at");

                entity.Property(e => e.Title)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("title");

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.Tests)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__test__class_id__02FC7413");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Tests)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK__test__resource_i__03F0984C");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
