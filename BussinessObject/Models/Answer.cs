﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Answer
    {
        public Answer()
        {
            StudentAnswers = new HashSet<StudentAnswer>();
        }

        public Guid AnswerId { get; set; }
        public Guid QuestionId { get; set; }
        public Guid? ResourceId { get; set; }
        public int? AnswerOrder { get; set; }
        public string? Content { get; set; }
        public bool? IsCorrect { get; set; }

        public virtual Question Question { get; set; } = null!;
        public virtual Resource? Resource { get; set; }
        public virtual ICollection<StudentAnswer> StudentAnswers { get; set; }
    }
}
