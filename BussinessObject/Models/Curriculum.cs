﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Curriculum
    {
        public Curriculum()
        {
            Chapters = new HashSet<Chapter>();
        }

        public Guid CurriculumId { get; set; }
        public string CurriculumTitle { get; set; } = null!;
        public Guid ClassId { get; set; }

        public virtual Class Class { get; set; } = null!;
        public virtual ICollection<Chapter> Chapters { get; set; }
    }
}
