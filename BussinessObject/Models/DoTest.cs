﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class DoTest
    {
        public Guid TestId { get; set; }
        public Guid AccountId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? FinishTime { get; set; }
        public double? Score { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual Test Test { get; set; } = null!;
    }
}
