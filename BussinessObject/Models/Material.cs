﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Material
    {
        public Guid ChapterId { get; set; }
        public Guid MaterialId { get; set; }
        public Guid? ResourceId { get; set; }
        public string MaterialTitle { get; set; } = null!;

        public virtual Chapter Chapter { get; set; } = null!;
        public virtual Resource? Resource { get; set; }
    }
}
