﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Chapter
    {
        public Chapter()
        {
            Materials = new HashSet<Material>();
        }

        public Guid ChapterId { get; set; }
        public Guid CurriculumId { get; set; }
        public string ChapterTitle { get; set; } = null!;

        public virtual Curriculum Curriculum { get; set; } = null!;
        public virtual ICollection<Material> Materials { get; set; }
    }
}
