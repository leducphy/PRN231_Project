﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Test
    {
        public Test()
        {
            DoTests = new HashSet<DoTest>();
            Questions = new HashSet<Question>();
        }

        public Guid TestId { get; set; }
        public Guid ClassId { get; set; }
        public Guid? ResourceId { get; set; }
        public string Title { get; set; } = null!;
        public string? Description { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime? EndAt { get; set; }
        public int? Duration { get; set; }
        public bool? AllowReview { get; set; }
        public DateTime? CreateTime { get; set; }

        public virtual Class Class { get; set; } = null!;
        public virtual Resource? Resource { get; set; }
        public virtual ICollection<DoTest> DoTests { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
    }
}
