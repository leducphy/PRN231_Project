﻿using System;
using System.Collections.Generic;

namespace BussinessObject.Models
{
    public partial class Question
    {
        public Question()
        {
            Answers = new HashSet<Answer>();
            StudentAnswers = new HashSet<StudentAnswer>();
        }

        public Guid QuestionId { get; set; }
        public Guid TestId { get; set; }
        public Guid? ResourceId { get; set; }
        public int? QuestionOrder { get; set; }
        public string? Content { get; set; }

        public virtual Resource? Resource { get; set; }
        public virtual Test Test { get; set; } = null!;
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<StudentAnswer> StudentAnswers { get; set; }
    }
}
