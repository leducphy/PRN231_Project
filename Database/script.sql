USE [master]
GO
/****** Object:  Database [online_learning]    Script Date: 1/23/2024 1:27:07 PM ******/
CREATE DATABASE [online_learning]
GO

USE [online_learning]
GO



ALTER DATABASE [online_learning] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [online_learning] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [online_learning] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [online_learning] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [online_learning] SET ARITHABORT OFF 
GO
ALTER DATABASE [online_learning] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [online_learning] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [online_learning] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [online_learning] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [online_learning] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [online_learning] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [online_learning] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [online_learning] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [online_learning] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [online_learning] SET  ENABLE_BROKER 
GO
ALTER DATABASE [online_learning] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [online_learning] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [online_learning] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [online_learning] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [online_learning] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [online_learning] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [online_learning] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [online_learning] SET RECOVERY FULL 
GO
ALTER DATABASE [online_learning] SET  MULTI_USER 
GO
ALTER DATABASE [online_learning] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [online_learning] SET DB_CHAINING OFF 
GO
ALTER DATABASE [online_learning] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [online_learning] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [online_learning] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [online_learning] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'online_learning', N'ON'
GO
ALTER DATABASE [online_learning] SET QUERY_STORE = ON
GO
ALTER DATABASE [online_learning] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [online_learning]
GO
/****** Object:  Table [dbo].[account]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[account](
	[account_id] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[password] [char](64) NOT NULL,
	[role] [tinyint] NOT NULL,
	[profile_picture] [varchar](100) NULL,
	[verified_at] [datetime] NULL,
	[verification_code] [uniqueidentifier] NULL,
	[create_time] [datetime] NULL,
	[password_reset_token_expires] [datetime] NULL,
	[password_reset_token] [uniqueidentifier] NULL,
	[locked] [bit] NULL,
	[dob] [datetime] NULL,
	[gender] [bit] NULL,
	[phone] [varchar](20) NULL,
	[address] [nchar](255) NULL,
	[school] [nchar](255) NULL,
 CONSTRAINT [PK__account__46A222CD305572BD] PRIMARY KEY CLUSTERED 
(
	[account_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UQ__account__AB6E61647DEED14A] UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[answer]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[answer](
	[answer_id] [uniqueidentifier] NOT NULL,
	[question_id] [uniqueidentifier] NOT NULL,
	[resource_id] [uniqueidentifier] NULL,
	[answer_order] [int] NULL,
	[content] [text] NULL,
	[isCorrect] [bit] NULL,
 CONSTRAINT [PK__answer__3372431820D9E211] PRIMARY KEY CLUSTERED 
(
	[answer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[chapter]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chapter](
	[chapter_id] [uniqueidentifier] NOT NULL,
	[curriculum_id] [uniqueidentifier] NOT NULL,
	[chapter_title] [nchar](255) NOT NULL,
 CONSTRAINT [PK_chapter] PRIMARY KEY CLUSTERED 
(
	[chapter_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[class]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[class](
	[class_id] [uniqueidentifier] NOT NULL,
	[account_id] [uniqueidentifier] NOT NULL,
	[name] [varchar](100) NOT NULL,
	[code] [char](5) NOT NULL,
	[enroll_approve] [bit] NULL,
	[class_picture] [varchar](100) NULL,
	[hidden] [bit] NULL,
	[create_time] [datetime] NULL,
	[class_description] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[class_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[comment]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comment](
	[comment_id] [uniqueidentifier] NOT NULL,
	[account_id] [uniqueidentifier] NOT NULL,
	[post_id] [uniqueidentifier] NOT NULL,
	[resource_id] [uniqueidentifier] NULL,
	[content] [text] NOT NULL,
	[create_time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[comment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[curriculum]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[curriculum](
	[curriculum_id] [uniqueidentifier] NOT NULL,
	[curriculum_title] [nvarchar](255) NOT NULL,
	[class_id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_curriculum] PRIMARY KEY CLUSTERED 
(
	[curriculum_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[do_test]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[do_test](
	[test_id] [uniqueidentifier] NOT NULL,
	[account_id] [uniqueidentifier] NOT NULL,
	[start_time] [datetime] NULL,
	[finish_time] [datetime] NULL,
	[score] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[test_id] ASC,
	[account_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[enrollment]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[enrollment](
	[account_id] [uniqueidentifier] NOT NULL,
	[class_id] [uniqueidentifier] NOT NULL,
	[accepted] [bit] NULL,
	[enroll_time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[account_id] ASC,
	[class_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[feedback]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback](
	[feedback_id] [uniqueidentifier] NOT NULL,
	[account_id] [uniqueidentifier] NOT NULL,
	[title] [varchar](300) NOT NULL,
	[content] [text] NOT NULL,
	[response] [text] NULL,
	[create_time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[feedback_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[material]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[material](
	[chapter_id] [uniqueidentifier] NOT NULL,
	[material_id] [uniqueidentifier] NOT NULL,
	[resource_id] [uniqueidentifier] NULL,
	[material_title] [nchar](255) NOT NULL,
 CONSTRAINT [PK_material] PRIMARY KEY CLUSTERED 
(
	[material_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[notification]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[notification](
	[notification_id] [uniqueidentifier] NOT NULL,
	[account_id] [uniqueidentifier] NOT NULL,
	[class_id] [uniqueidentifier] NULL,
	[target] [uniqueidentifier] NULL,
	[type] [tinyint] NULL,
	[title] [varchar](100) NOT NULL,
	[redirect_url] [varchar](100) NULL,
	[content] [varchar](200) NOT NULL,
	[create_time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[notification_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[post]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[post](
	[post_id] [uniqueidentifier] NOT NULL,
	[account_id] [uniqueidentifier] NOT NULL,
	[class_id] [uniqueidentifier] NOT NULL,
	[pin] [bit] NULL,
	[content] [text] NOT NULL,
	[create_time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[post_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[question]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[question](
	[question_id] [uniqueidentifier] NOT NULL,
	[test_id] [uniqueidentifier] NOT NULL,
	[resource_id] [uniqueidentifier] NULL,
	[question_order] [int] NULL,
	[content] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[question_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[resource]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[resource](
	[resource_id] [uniqueidentifier] NOT NULL,
	[post_id] [uniqueidentifier] NULL,
	[account_id] [uniqueidentifier] NOT NULL,
	[url] [varchar](max) NOT NULL,
	[thumbnail] [varchar](300) NULL,
	[mime_type] [varchar](100) NOT NULL,
	[deleted] [bit] NULL,
 CONSTRAINT [PK__resource__4985FC7320ED0F56] PRIMARY KEY CLUSTERED 
(
	[resource_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[student_answer]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[student_answer](
	[account_id] [uniqueidentifier] NOT NULL,
	[question_id] [uniqueidentifier] NOT NULL,
	[answer_id] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[account_id] ASC,
	[question_id] ASC,
	[answer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[test]    Script Date: 3/10/2024 10:19:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[test](
	[test_id] [uniqueidentifier] NOT NULL,
	[class_id] [uniqueidentifier] NOT NULL,
	[resource_id] [uniqueidentifier] NULL,
	[title] [varchar](300) NOT NULL,
	[description] [text] NULL,
	[start_at] [datetime] NOT NULL,
	[end_at] [datetime] NULL,
	[duration] [int] NULL,
	[allow_review] [bit] NULL,
	[create_time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[test_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[account] ADD  CONSTRAINT [DF__account__account__5441852A]  DEFAULT (newid()) FOR [account_id]
GO
ALTER TABLE [dbo].[account] ADD  CONSTRAINT [DF__account__create___5535A963]  DEFAULT (getdate()) FOR [create_time]
GO
ALTER TABLE [dbo].[account] ADD  CONSTRAINT [DF__account__locked__5629CD9C]  DEFAULT ((0)) FOR [locked]
GO
ALTER TABLE [dbo].[answer] ADD  CONSTRAINT [DF__answer__answer_i__571DF1D5]  DEFAULT (newid()) FOR [answer_id]
GO
ALTER TABLE [dbo].[answer] ADD  CONSTRAINT [DF__answer__correct__5812160E]  DEFAULT ((0)) FOR [isCorrect]
GO
ALTER TABLE [dbo].[chapter] ADD  CONSTRAINT [DF_chapter_chapter_id]  DEFAULT (newid()) FOR [chapter_id]
GO
ALTER TABLE [dbo].[class] ADD  DEFAULT (newid()) FOR [class_id]
GO
ALTER TABLE [dbo].[class] ADD  DEFAULT ((1)) FOR [enroll_approve]
GO
ALTER TABLE [dbo].[class] ADD  DEFAULT ((0)) FOR [hidden]
GO
ALTER TABLE [dbo].[class] ADD  DEFAULT (getdate()) FOR [create_time]
GO
ALTER TABLE [dbo].[comment] ADD  DEFAULT (newid()) FOR [comment_id]
GO
ALTER TABLE [dbo].[comment] ADD  DEFAULT (getdate()) FOR [create_time]
GO
ALTER TABLE [dbo].[curriculum] ADD  CONSTRAINT [DF_curriculum_curriculum_id]  DEFAULT (newid()) FOR [curriculum_id]
GO
ALTER TABLE [dbo].[do_test] ADD  DEFAULT (getdate()) FOR [start_time]
GO
ALTER TABLE [dbo].[feedback] ADD  DEFAULT (newid()) FOR [feedback_id]
GO
ALTER TABLE [dbo].[feedback] ADD  DEFAULT (getdate()) FOR [create_time]
GO
ALTER TABLE [dbo].[material] ADD  CONSTRAINT [DF_material_material_id]  DEFAULT (newid()) FOR [material_id]
GO
ALTER TABLE [dbo].[notification] ADD  DEFAULT (newid()) FOR [notification_id]
GO
ALTER TABLE [dbo].[notification] ADD  DEFAULT (getdate()) FOR [create_time]
GO
ALTER TABLE [dbo].[post] ADD  DEFAULT (newid()) FOR [post_id]
GO
ALTER TABLE [dbo].[post] ADD  DEFAULT ((0)) FOR [pin]
GO
ALTER TABLE [dbo].[post] ADD  DEFAULT (getdate()) FOR [create_time]
GO
ALTER TABLE [dbo].[question] ADD  DEFAULT (newid()) FOR [question_id]
GO
ALTER TABLE [dbo].[resource] ADD  CONSTRAINT [DF__resource__resour__6754599E]  DEFAULT (newid()) FOR [resource_id]
GO
ALTER TABLE [dbo].[resource] ADD  CONSTRAINT [DF__resource__delete__68487DD7]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[test] ADD  DEFAULT (newid()) FOR [test_id]
GO
ALTER TABLE [dbo].[test] ADD  DEFAULT ((1)) FOR [allow_review]
GO
ALTER TABLE [dbo].[test] ADD  DEFAULT (getdate()) FOR [create_time]
GO
ALTER TABLE [dbo].[answer]  WITH CHECK ADD  CONSTRAINT [FK__answer__question__6C190EBB] FOREIGN KEY([question_id])
REFERENCES [dbo].[question] ([question_id])
GO
ALTER TABLE [dbo].[answer] CHECK CONSTRAINT [FK__answer__question__6C190EBB]
GO
ALTER TABLE [dbo].[answer]  WITH CHECK ADD  CONSTRAINT [FK__answer__resource__6D0D32F4] FOREIGN KEY([resource_id])
REFERENCES [dbo].[resource] ([resource_id])
GO
ALTER TABLE [dbo].[answer] CHECK CONSTRAINT [FK__answer__resource__6D0D32F4]
GO
ALTER TABLE [dbo].[chapter]  WITH CHECK ADD  CONSTRAINT [FK_chapter_curriculum] FOREIGN KEY([curriculum_id])
REFERENCES [dbo].[curriculum] ([curriculum_id])
GO
ALTER TABLE [dbo].[chapter] CHECK CONSTRAINT [FK_chapter_curriculum]
GO
ALTER TABLE [dbo].[class]  WITH CHECK ADD  CONSTRAINT [FK__class__account_i__6E01572D] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[class] CHECK CONSTRAINT [FK__class__account_i__6E01572D]
GO
ALTER TABLE [dbo].[comment]  WITH CHECK ADD  CONSTRAINT [FK__comment__account__6EF57B66] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[comment] CHECK CONSTRAINT [FK__comment__account__6EF57B66]
GO
ALTER TABLE [dbo].[comment]  WITH CHECK ADD FOREIGN KEY([post_id])
REFERENCES [dbo].[post] ([post_id])
GO
ALTER TABLE [dbo].[comment]  WITH CHECK ADD  CONSTRAINT [FK__comment__resourc__70DDC3D8] FOREIGN KEY([resource_id])
REFERENCES [dbo].[resource] ([resource_id])
GO
ALTER TABLE [dbo].[comment] CHECK CONSTRAINT [FK__comment__resourc__70DDC3D8]
GO
ALTER TABLE [dbo].[curriculum]  WITH CHECK ADD  CONSTRAINT [FK_curriculum_class] FOREIGN KEY([class_id])
REFERENCES [dbo].[class] ([class_id])
GO
ALTER TABLE [dbo].[curriculum] CHECK CONSTRAINT [FK_curriculum_class]
GO
ALTER TABLE [dbo].[do_test]  WITH CHECK ADD  CONSTRAINT [FK__do_test__account__71D1E811] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[do_test] CHECK CONSTRAINT [FK__do_test__account__71D1E811]
GO
ALTER TABLE [dbo].[do_test]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[test] ([test_id])
GO
ALTER TABLE [dbo].[enrollment]  WITH CHECK ADD  CONSTRAINT [FK__enrollmen__accou__73BA3083] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[enrollment] CHECK CONSTRAINT [FK__enrollmen__accou__73BA3083]
GO
ALTER TABLE [dbo].[enrollment]  WITH CHECK ADD FOREIGN KEY([class_id])
REFERENCES [dbo].[class] ([class_id])
GO
ALTER TABLE [dbo].[feedback]  WITH CHECK ADD  CONSTRAINT [FK__feedback__accoun__75A278F5] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[feedback] CHECK CONSTRAINT [FK__feedback__accoun__75A278F5]
GO
ALTER TABLE [dbo].[material]  WITH CHECK ADD  CONSTRAINT [FK_material_chapter] FOREIGN KEY([chapter_id])
REFERENCES [dbo].[chapter] ([chapter_id])
GO
ALTER TABLE [dbo].[material] CHECK CONSTRAINT [FK_material_chapter]
GO
ALTER TABLE [dbo].[material]  WITH CHECK ADD  CONSTRAINT [FK_material_resource] FOREIGN KEY([resource_id])
REFERENCES [dbo].[resource] ([resource_id])
GO
ALTER TABLE [dbo].[material] CHECK CONSTRAINT [FK_material_resource]
GO
ALTER TABLE [dbo].[notification]  WITH CHECK ADD  CONSTRAINT [FK__notificat__accou__76969D2E] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[notification] CHECK CONSTRAINT [FK__notificat__accou__76969D2E]
GO
ALTER TABLE [dbo].[notification]  WITH CHECK ADD FOREIGN KEY([class_id])
REFERENCES [dbo].[class] ([class_id])
GO
ALTER TABLE [dbo].[notification]  WITH CHECK ADD  CONSTRAINT [FK__notificat__targe__787EE5A0] FOREIGN KEY([target])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[notification] CHECK CONSTRAINT [FK__notificat__targe__787EE5A0]
GO
ALTER TABLE [dbo].[post]  WITH CHECK ADD  CONSTRAINT [FK__post__account_id__797309D9] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[post] CHECK CONSTRAINT [FK__post__account_id__797309D9]
GO
ALTER TABLE [dbo].[post]  WITH CHECK ADD FOREIGN KEY([class_id])
REFERENCES [dbo].[class] ([class_id])
GO
ALTER TABLE [dbo].[question]  WITH CHECK ADD  CONSTRAINT [FK__question__resour__7D439ABD] FOREIGN KEY([resource_id])
REFERENCES [dbo].[resource] ([resource_id])
GO
ALTER TABLE [dbo].[question] CHECK CONSTRAINT [FK__question__resour__7D439ABD]
GO
ALTER TABLE [dbo].[question]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[test] ([test_id])
GO
ALTER TABLE [dbo].[resource]  WITH CHECK ADD  CONSTRAINT [FK__resource__accoun__7F2BE32F] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[resource] CHECK CONSTRAINT [FK__resource__accoun__7F2BE32F]
GO
ALTER TABLE [dbo].[resource]  WITH CHECK ADD  CONSTRAINT [FK_resource_post] FOREIGN KEY([post_id])
REFERENCES [dbo].[post] ([post_id])
GO
ALTER TABLE [dbo].[resource] CHECK CONSTRAINT [FK_resource_post]
GO
ALTER TABLE [dbo].[student_answer]  WITH CHECK ADD  CONSTRAINT [FK__student_a__accou__00200768] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[student_answer] CHECK CONSTRAINT [FK__student_a__accou__00200768]
GO
ALTER TABLE [dbo].[student_answer]  WITH CHECK ADD  CONSTRAINT [FK__student_a__answe__01142BA1] FOREIGN KEY([answer_id])
REFERENCES [dbo].[answer] ([answer_id])
GO
ALTER TABLE [dbo].[student_answer] CHECK CONSTRAINT [FK__student_a__answe__01142BA1]
GO
ALTER TABLE [dbo].[student_answer]  WITH CHECK ADD FOREIGN KEY([question_id])
REFERENCES [dbo].[question] ([question_id])
GO
ALTER TABLE [dbo].[test]  WITH CHECK ADD FOREIGN KEY([class_id])
REFERENCES [dbo].[class] ([class_id])
GO
ALTER TABLE [dbo].[test]  WITH CHECK ADD  CONSTRAINT [FK__test__resource_i__03F0984C] FOREIGN KEY([resource_id])
REFERENCES [dbo].[resource] ([resource_id])
GO
ALTER TABLE [dbo].[test] CHECK CONSTRAINT [FK__test__resource_i__03F0984C]
GO
USE [master]
GO
ALTER DATABASE [online_learning] SET  READ_WRITE 
GO
