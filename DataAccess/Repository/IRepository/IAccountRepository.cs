﻿using BussinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository.IRepository
{
    public interface IAccountRepository
    {
        Task<bool> Add(Account account, CancellationToken cancellationToken = default);
        Task<List<Account>> GetAll();
        Task<Account> GetAccountById(Guid aid);
        Task<bool> CheckAddExistEmail(string email, CancellationToken cancellationToken = default);
        Task<Account> Login(string username,  string password, CancellationToken cancellationToken = default);
        Task<bool> Verify(Guid verifyCode, CancellationToken cancellationToken = default);
        Task<Guid> ForgotPassword(string email, CancellationToken cancellationToken = default);
        Task<bool> ResetPassword(Guid resetToken, string newPass, CancellationToken cancellationToken = default);
        Task<bool> Update(Account account);
        Task<List<AccountDTO>> GetAccountsByClassId(Guid cid);
    }
}
