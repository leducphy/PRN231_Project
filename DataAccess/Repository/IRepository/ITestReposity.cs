﻿using BussinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository.IRepository
{
    public interface ITestReposity
    {
        public Task<Test> addTest(Test test);
        public List<Test> getTests();
        public List<Test> getTests(Guid? classId);
        public List<Test> getTestsByClassId(Guid cid);
        public Test GetTest(Guid testId);
        public Test UpdateTest(Test test);
    }
}
