﻿using BussinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository.IRepository
{
    public interface ICurriculumnRepository
    {
        Task<bool> AddCurriculumn(Curriculum curriculum, CancellationToken cancellationToken = default);
        Task<bool> UpdateCurriculumn(Curriculum curriculum, CancellationToken cancellationToken = default);
        Task<Curriculum> ViewDetails(Guid curriculumID, CancellationToken cancellationToken = default);
    }
}
