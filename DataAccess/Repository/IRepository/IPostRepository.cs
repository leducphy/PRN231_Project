﻿using BussinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository.IRepository
{
    public interface IPostRepository
    {
        Task<Guid> CreatePost(string? classId, string? accountId, string? content, List<(string base64String, string mimeType)>? listBase64Picture);
    }
}
