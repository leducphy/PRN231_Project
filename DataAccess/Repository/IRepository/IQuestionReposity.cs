﻿using BussinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository.IRepository
{
    public interface IQuestionReposity
    {
        public Task<List<Question>> AddQuestions(List<Question> questions);
        public Task<List<Question>> GetQuestions(Guid TestId);
        public Task<Question> GetQuestion(Guid QuestionId);
    }
}
