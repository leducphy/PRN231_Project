﻿using BussinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository.IRepository
{
    public interface IAnswerRepository
    {
        public Task<List<Answer>> AddAnswers(List<Answer> answers);
        public Task<List<Answer>> GetAnswers(Guid QuestionId);
    }
}
