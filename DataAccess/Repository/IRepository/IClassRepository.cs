using BussinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccess.Repository.IRepository
{
    public interface IClassRepository 
    {
        Task<int> CheckJoinClass(string? classCode, string? accountId);
        Task<bool> LeaveClass(string? classCode, string? accountId);
        void AddClass(string className,bool enrollAppove, string classPicture , Guid accountId);
        IEnumerable<Class> GetClassesByAccountId(Guid accountId);
        IEnumerable<Class> GetClassesByAccountIdStudent(Guid accountId);
        IEnumerable<Account> GetStudentsByClassId(Guid classId , bool Accepted);
        Task<bool> AcceptStudent(Guid classId, List<Guid> accountIds);

        Task<Class> GetClassByCode(string? classCode);
        
        bool DeleteClass(Guid? classId);

        int CountClass();

        int CountAccountByRole(int role);

        Task<List<Class>> GetTop3Class();




    }
}
