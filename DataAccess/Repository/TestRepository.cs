﻿using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class TestRepository : ITestReposity
    {
        private readonly online_learningContext _context;
        public TestRepository(online_learningContext context)
        {
            _context = context;
        }
        public async Task<Test> addTest(Test test)
        {
            if (test == null)
            {
                return null;
            }
            else
            {
                await _context.Tests.AddAsync(test);
                await _context.SaveChangesAsync();
                return test;
            }
        }

        public  List<Test> getTests()
        {
            return _context.Tests.ToList();
        }

        public  List<Test> getTests(Guid? classId)
        {
            return _context.Tests.ToList();
        }

        public List<Test> getTestsByClassId(Guid cid)
        {
            return _context.Tests.Where(t => t.ClassId == cid).ToList();
        }

        public Test GetTest(Guid testId)
        {
            return _context.Tests.Include(t => t.Questions).ThenInclude(q => q.Answers).FirstOrDefault(t => t.TestId == testId);
        }

        public Test UpdateTest(Test test)
        {
            _context.Tests.Update(test);
            _context.SaveChanges();
            return test;
        }
    }
}
