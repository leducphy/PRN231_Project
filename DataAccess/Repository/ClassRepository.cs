﻿using AutoMapper;
using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccess.Repository
{
    public class ClassRepository : IClassRepository
    {
        private readonly online_learningContext _context;
        private readonly IMapper _mapper;

        public ClassRepository(online_learningContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<int> CheckJoinClass(string? classCode, string? accountId)
        {
            // check xem mã code có tồn tại hay không
            var resCheckCode = await _context.Classes?.FirstOrDefaultAsync(x => x.Code == classCode);

            // Nếu có mã code lớp 
            if (resCheckCode != null)
            {
                // Kiểm tra xem học sinh đó đã join lớp này hay chưa
                var resCheckEnroll = await _context.Enrollments?.FirstOrDefaultAsync(x => x.AccountId == Guid.Parse(accountId) && x.ClassId == resCheckCode.ClassId);

                var enrollmentObject = new Enrollment();
                enrollmentObject.AccountId = Guid.Parse(accountId);
                enrollmentObject.ClassId = resCheckCode.ClassId;
                enrollmentObject.EnrollTime = DateTime.Now;

                // Nếu join rồi thì cảnh báo
                if (resCheckEnroll != null)
                {
                    return 2;
                }

                // Nếu chưa join thì add vào lớp
                // case 1: lớp có bật kiểm duyệt
                if((bool)resCheckCode.EnrollApprove)
                {
                    enrollmentObject.Accepted = false;
                    _context.Enrollments.Add(enrollmentObject);
                    await _context.SaveChangesAsync(); // Save the changes to the database
                    return 3;
                }
                // case 2: lớp không bật kiểm duyệt
                enrollmentObject.Accepted = true;
                _context.Enrollments.Add(enrollmentObject);
                await _context.SaveChangesAsync(); // Save the changes to the database
                return 4;
            }
            return 1;
        }

        public async Task<bool> LeaveClass(string? classCode, string? accountId)
        {
            // check xem mã code có tồn tại hay không
            var resCheckCode = await _context.Classes?.FirstOrDefaultAsync(x => x.Code == classCode);

            // Nếu có mã code lớp 
            if (resCheckCode != null)
            {
                // Kiểm tra xem học sinh đó đã join lớp này hay chưa
                var resCheckEnroll = await _context.Enrollments?.FirstOrDefaultAsync(x => x.AccountId == Guid.Parse(accountId) && x.ClassId == resCheckCode.ClassId);

                if (resCheckEnroll != null)
                {
                    var enrollmentObject = new Enrollment();
                    enrollmentObject.AccountId = Guid.Parse(accountId);
                    enrollmentObject.ClassId = resCheckCode.ClassId;
                    _context.Enrollments.Remove(enrollmentObject);
                    await _context.SaveChangesAsync(); // Save the changes to the database
                    return true;
                }
            }
            return false;
        }

        public void AddClass(string className,  bool enrollAppove, string classPicture , Guid accountId)
        {
            string classCode = GenerateUniqueCode();
            var newClass = new Class
            {
                ClassId = Guid.NewGuid(),
                AccountId = accountId,
                Name = className,
                Code = classCode,
                EnrollApprove = enrollAppove,
                Hidden = false,
                CreateTime = DateTime.Now,
                ClassPicture = classPicture,
            };
            _context.Classes.Add(newClass);
            _context.SaveChanges();
        }
        private string GenerateUniqueCode()
        {
            string classCode = GenerateRandomCode(); ;
            while (_context.Classes.Any(c => c.Code == classCode))
            {
                classCode = GenerateRandomCode();
            }
            return classCode;
        }
        private string GenerateRandomCode()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, 5)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public IEnumerable<Class> GetClassesByAccountId(Guid accountId)
        {
            return _context.Classes.Where(c => c.AccountId == accountId).Where(c => c.Hidden == false).ToList();
        }

        public async Task<Class> GetClassByCode(string? classCode)
        {
            return await _context.Classes.FirstOrDefaultAsync(c => c.Code.ToLower() == classCode.ToLower());
        }
        public IEnumerable<Class> GetClassesByAccountIdStudent(Guid accountId)
        {
            return _context.Classes.Where(c => c.Enrollments.Any(e => e.AccountId == accountId && e.Accepted )).ToList();
        }

        public IEnumerable<Account> GetStudentsByClassId(Guid classId , bool Accepted)
        {
            return _context.Accounts.Where(a => a.Enrollments.Any(e => e.ClassId == classId && e.Accepted == Accepted)).ToList();        
        }

        public async Task<bool> AcceptStudent(Guid classId, List<Guid> accountIds)
        {
            var enrollments = _context.Enrollments.Where(e => e.Class.ClassId == classId && accountIds.Contains(e.Account.AccountId));

            foreach (var enrollment in enrollments)
            {
                enrollment.Accepted = true;
            }

            await _context.SaveChangesAsync();

            return true;
        }

        public bool DeleteClass(Guid? classId)
        {

            var classObj = _context.Classes.FirstOrDefault(c => c.ClassId == classId);
            classObj.Hidden = true;
            return true;
        }

        public int CountClass()
        {
            return _context.Classes.Count();
        }

        public int  CountAccountByRole(int role)
        {
            return _context.Accounts.Where(a => a.Role == role).Count();   
        }

        public async Task<List<Class>> GetTop3Class()
        {
            var topClasses = _context.Classes
         .OrderByDescending(c => c.Enrollments.Count)
         .Take(3)
         .ToList();


            return topClasses;
        }
    }
}
