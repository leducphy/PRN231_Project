﻿using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class AnswerRepository : IAnswerRepository
    {
        private readonly online_learningContext _context;
        public AnswerRepository(online_learningContext context)
        {
            _context = context;
        }
        public async Task<List<Answer>> AddAnswers(List<Answer> answers)
        {
            await _context.Answers.AddRangeAsync(answers);
            await _context.SaveChangesAsync();
            return answers;
        }

        public Task<List<Answer>> GetAnswers(Guid QuestionId)
        {
            var answers = _context.Answers.Where(ans => ans.QuestionId == QuestionId).ToListAsync();
            return answers;
        }
    }
}
