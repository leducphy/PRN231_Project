﻿using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class QuestionRepository : IQuestionReposity
    {
        private readonly online_learningContext _context;
        public QuestionRepository(online_learningContext context)
        {
            _context = context;
        }

        public async Task<List<Question>> AddQuestions(List<Question> questions)
        {
            await _context.Questions.AddRangeAsync(questions);
            await _context.SaveChangesAsync();
            return questions;
        }

        public Task<Question> GetQuestion(Guid QuestionId)
        {
            return _context.Questions.FirstOrDefaultAsync(q => q.QuestionId == QuestionId);
        }

        public Task<List<Question>> GetQuestions(Guid TestId)
        {
            return _context.Questions.Where(q => q.TestId == TestId).ToListAsync();
        }
    }
}
