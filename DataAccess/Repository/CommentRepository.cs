﻿using AutoMapper;
using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class CommentRepository : ICommentRepository
    {
        private readonly online_learningContext _context;
        private readonly IMapper _mapper;

        public CommentRepository(online_learningContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> CreateComment(string? postId, string? accountId, string? content)
    {
            var commentObject = new Comment();
            try
            {
                var commentId = Guid.NewGuid();
                commentObject.PostId = Guid.Parse(postId);
                commentObject.AccountId = Guid.Parse(accountId);
                commentObject.CommentId = commentId;
                commentObject.Content = content;
                commentObject.CreateTime = DateTime.Now;

                _context.Comments.Add(commentObject);
                _context.SaveChanges();

            }
            catch (Exception)
            {
                throw;
            }

            return true;
        }
    }
}
