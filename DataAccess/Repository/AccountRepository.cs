﻿using AutoMapper;
using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject.Utility;
using System.Security.Principal;

namespace DataAccess.Repository
{
    public class AccountRepository : IAccountRepository
    {
        private readonly online_learningContext _context;
        private readonly IMapper _mapper;

        public AccountRepository(online_learningContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Add(Account account, CancellationToken cancellationToken = default)
        {
            try
            {
                await _context.Accounts.AddAsync(account);
                await _context.SaveChangesAsync(cancellationToken);
                return true; // Success
            }
            catch (Exception ex)
            {
                return false; // Failure
            }
        }

        public async Task<List<Account>> GetAll()
        {
            return await _context.Accounts.ToListAsync();
        }

        public async Task<bool> CheckAddExistEmail(string email, CancellationToken cancellationToken = default)
        {
            List<Account> listS = await _context.Accounts.ToListAsync();
            foreach (var cus in listS)
            {
                if (email.Trim() == cus.Email.Trim())
                {
                    return false;
                }
            }
            return true;
        }
        public async Task<Account> GetAccountById(Guid aid)
        {
            var account = await _context.Accounts.FirstOrDefaultAsync(i => i.AccountId == aid);
            if (account == null)
            {
                throw new ArgumentException("can not find!!");
            }
            return account;
        }

        public async Task<Account> Login(string email, string passwordHashed, CancellationToken cancellationToken = default)
        {
            var accountLogin = await _context.Accounts.FirstOrDefaultAsync(i => i.Email == email && i.Password == passwordHashed);
            if (accountLogin == null) throw new ArgumentException("Login fail!");
            return accountLogin;
        }

        public async Task<bool> Verify(Guid verifyCode, CancellationToken cancellationToken = default)
        {
            var user = await _context.Accounts.FirstOrDefaultAsync(u => u.VerificationCode == verifyCode);
            if (user == null)
            {
                return false;
                throw new ArgumentException("invalid token");
            }
            user.VerificationCode = null;
            user.VerifiedAt = DateTime.UtcNow;

            try
            {
                await _context.SaveChangesAsync(cancellationToken);
                return true; // Verification successful
            }
            catch (Exception ex)
            {
                // Handle the exception (log, throw, etc.) based on your application's needs
                return false; // Verification failed due to save changes error
            }
        }

        public async Task<Guid> ForgotPassword(string email, CancellationToken cancellationToken)
        {
            var resetToken = Guid.NewGuid();

            var user = await _context.Accounts.FirstOrDefaultAsync(u => u.Email == email, cancellationToken: cancellationToken);

            if (user == null)
            {
                throw new ArgumentException("User not found");
            }

            user.PasswordResetToken = resetToken;
            user.PasswordResetTokenExpires = DateTime.Now.AddMinutes(15);

            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Handle concurrency exception if needed
                // Log the exception for further investigation
                // Retry logic can be implemented here
                throw;
            }

            return resetToken;
        }


        public async Task<bool> ResetPassword(Guid resetToken, string newPass, CancellationToken cancellationToken = default)
        {
            var user = await _context.Accounts.FirstOrDefaultAsync(u => u.PasswordResetToken == resetToken, cancellationToken);
            if (user == null)
            {
                return false;
                throw new ArgumentException("invalid token");
            }
            user.Password = newPass;
            user.PasswordResetToken = null;
            user.PasswordResetTokenExpires = null;
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> Update(Account account)
        {
            try
            {
                string sql = $"UPDATE account SET name = '{account.Name}', dob = '{account.Dob}', gender = '{account.Gender}', " +
                    $"phone = '{account.Phone}', address = '{account.Address}', school = '{account.School}' WHERE account_id = '{account.AccountId}'";
                await _context.Database.ExecuteSqlRawAsync(sql);
                await _context.SaveChangesAsync();
                return true; // Success
            }
            catch (Exception ex)
            {
                return false; // Failure
            }
        }

        public async Task<List<AccountDTO>> GetAccountsByClassId(Guid cid)
        {
            var accounts = await (
                from c in _context.Classes
                join e in _context.Enrollments on c.ClassId equals e.ClassId
                join a in _context.Accounts on e.AccountId equals a.AccountId
                where c.ClassId == cid
                select new AccountDTO
                    {
                        AccountId = a.AccountId,
                        Name = a.Name,
                        Role = a.Role,
                        Tests = (ICollection<TestDTO>)(
                            from dt in _context.DoTests
                            join t in _context.Tests on dt.TestId equals t.TestId
                            where dt.AccountId == a.AccountId
                            select new TestDTO
                                {
                                    TestId = dt.TestId,
                                    Title = t.Title,
                                    Score = dt.Score
                                }
                            ).ToList()
                        }
                ).ToListAsync();
            return accounts;
        }
    }

    public class AccountDTO
    {
        public Guid AccountId { get; set; }
        public string Name { get; set; } = null!;
        public byte Role { get; set; }
        public virtual ICollection<TestDTO> Tests { get; set; }
    }

    public class TestDTO
    {
        public Guid TestId { get; set; }
        public string Title { get; set; } = null!;
        public double? Score { get; set; }
    }
}
