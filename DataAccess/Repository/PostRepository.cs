﻿using AutoMapper;
using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class PostRepository : IPostRepository
    {
        private readonly online_learningContext _context;
        private readonly IMapper _mapper;

        public PostRepository(online_learningContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
      

        public async Task<Guid> CreatePost(string? classId, string? accountId, string? content, List<(string base64String, string mimeType)>? listBase64Picture)
        {
            var postObject = new Post();
            try
            {
                    var classByCode = await _context.Classes?.FirstOrDefaultAsync(x => x.ClassId == Guid.Parse(classId));

                    if (classByCode != null)
                    {
                        var postId = Guid.NewGuid();
                        postObject.PostId = postId;
                        postObject.AccountId = Guid.Parse(accountId);
                        postObject.ClassId = classByCode.ClassId;
                        postObject.Content = content;
                        postObject.CreateTime = DateTime.Now;

                        _context.Posts.Add(postObject);
                        _context.SaveChanges();
                    }

                    var resourceList = new List<Resource>();
                    foreach (var item in listBase64Picture)
                    {
                        var objectResource = new Resource();
                        objectResource.AccountId = Guid.Parse(accountId);
                        objectResource.PostId = postObject.PostId;
                        objectResource.ResourceId = Guid.NewGuid();
                        objectResource.Url = item.base64String;
                        objectResource.MimeType = item.mimeType;

                        resourceList.Add(objectResource);
                    }

                     _context.Resources.AddRange(resourceList);
                     _context.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }

            return postObject.PostId;
        }
    }
}
