﻿using AutoMapper;
using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class CurriculumnRepository : ICurriculumnRepository
    {

        private readonly online_learningContext _context;
        private readonly IMapper _mapper;

        public CurriculumnRepository(online_learningContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<bool> AddCurriculumn(Curriculum curriculum, CancellationToken cancellationToken)
        {
            try
            {
                await _context.Curricula.AddAsync(curriculum);
                await _context.SaveChangesAsync(cancellationToken);
                return true; // Success
            }
            catch (Exception ex)
            {
                return false; // Failure
            }
        }

        public Task<bool> UpdateCurriculumn(Curriculum curriculum, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<Curriculum> ViewDetails(Guid classID, CancellationToken cancellationToken)
        {
            var curriculum = await _context.Curricula
                .Include(c => c.Chapters)  
                .ThenInclude(ch => ch.Materials)  
                .FirstOrDefaultAsync(c => c.ClassId == classID, cancellationToken);

            return curriculum!;
        }
    }
}
