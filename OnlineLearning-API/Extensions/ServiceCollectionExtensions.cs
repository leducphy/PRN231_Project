﻿using DataAccess.Repository;
using DataAccess.Repository.IRepository;
using OnlineLearning_API.Configurations.Mappers;

namespace OnlineLearning_API.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration config)
        {
            // Đăng kí automapper
            services.AddAutoMapper(typeof(AutoMapperProfile));

            // Đăng kí mediatR
            //services.AddMediatR(Assembly.GetExecutingAssembly());
            //services.AddMediatR(typeof(ViewUserProfileQuery).Assembly);

            // Đăng kí repository
            services.AddScoped(typeof(IAccountRepository), typeof(AccountRepository));
            

            //Đăng kí service
            return services;
        }
    }
}
