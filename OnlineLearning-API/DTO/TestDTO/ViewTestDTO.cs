﻿using OnlineLearning_API.DTO.QuestionDTO;

namespace OnlineLearning_API.DTO.TestDTO
{
    public class ViewTestDTO
    {
        public Guid TestId { get; set; }
        public Guid ClassId { get; set; }
        public Guid? ResourceId { get; set; }
        public string Title { get; set; } = null!;
        public string? Description { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime? EndAt { get; set; }
        public int? Duration { get; set; }
        public bool? AllowReview { get; set; }
        public DateTime? CreateTime { get; set; }
        public ICollection<ViewQuestionDTO> Questions { get; set; }
    }
}
