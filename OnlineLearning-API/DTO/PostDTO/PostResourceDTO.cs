﻿using BussinessObject.Models;
using OnlineLearning_API.DTO.CommentDTO;
using OnlineLearning_API.DTO.ResourceDTO;

namespace OnlineLearning_API.DTO.PostDTO
{
    public class PostResourceDTO
    {
        public Guid PostId { get; set; }
        public string Content { get; set; } = null!;
        public string AccountName { get; set; } = null!;
        public DateTime? CreateTime { get; set; }

        public virtual ICollection<ResourcePostDTO> ResourcePostDTO { get; set; }
        public virtual ICollection<CommentPostDTO> CommentPostDTO { get; set; }
    }
}
