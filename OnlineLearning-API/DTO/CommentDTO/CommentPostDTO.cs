﻿using BussinessObject.Models;
using Microsoft.AspNetCore.Mvc;

namespace OnlineLearning_API.DTO.CommentDTO
{
    public class CommentPostDTO 
    {
        public string Content { get; set; } = null!;
        public Guid? AccountId { get; set; }
        public string AccountName { get; set; } = null!;
    }
}
