﻿using BussinessObject.Models;

namespace OnlineLearning_API.DTO.ResourceDTO
{
    public class ResourcePostDTO
    {
        public string Url { get; set; } = null!;
        public string MimeType { get; set; } = null!;

    }
}
