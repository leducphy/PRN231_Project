﻿using BussinessObject.Models;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace OnlineLearning_API.DTO
{
    public class AccountAddRequest
    {
        [JsonIgnore]
        public Guid AccountId { get; set; }
        [Required]
        public string Name { get; set; } = null!;
        [Required, EmailAddress]
        public string Email { get; set; } = null!;
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; } = null!;
        [Required]
        public byte Role { get; set; }
        public string? ProfilePicture { get; set; }
        [JsonIgnore]
        public Guid? VerificationCode { get; set; }
        [JsonIgnore]
        public DateTime? CreateTime { get; set; }
        [JsonIgnore]
        public bool? Locked { get; set; }
    }

    public class LoginRequestModel
    {
        [Required, EmailAddress]
        public string Email { get; set; } = string.Empty!;
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; } 
        bool isRememberMe { get; set; } = false;

    }

    public class ResetPasswordRequest
    {
        [Required]
        public Guid Token { get; set; } 
        [Required, MinLength(6, ErrorMessage = "Please enter at least 6 characters, dude!")]
        public string Password { get; set; } = string.Empty;
        [Required, Compare("Password")]
        public string ConfirmPassword { get; set; } = string.Empty;

    }

    public class ForgotPasswordRequest
    {
        [Required, EmailAddress]
        public string Email { get; set; }
    }

    public class EditProfileRequest
    {
        public string? Name { get; set; }
        public DateTime? Dob { get; set; }
        public bool? Gender { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public string? School { get; set; }
    }

    public class CreateCurriculumnRequest
    {
        [Required]
        public string CurriculumTitle { get; set; } = string.Empty;
        [Required]
        public Guid ClassId { get; set; }
        public List<ChapterAddRequest>? Chapters { get; set; }
    }

    public class ChapterAddRequest
    {
        [Required]
        public string ChapterTitle { get; set; } = string.Empty;
        public List<MaterialAddRequest>? Materials { get; set; }
    }

    public class MaterialAddRequest
    {
        public string MaterialTitle { get; set; } = string.Empty;

    }

}
