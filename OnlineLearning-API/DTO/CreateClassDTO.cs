﻿namespace OnlineLearning_API.DTO
{
    public class CreateClassDTO
    {
        public string Name { get; set; }
        public bool EnrollApprove { get; set; }
        public string ClassPicture { get; set; }

        public Guid AccountId { get; set; }
    }
}
