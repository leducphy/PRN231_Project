﻿using OnlineLearning_API.DTO.AnswerDTO;

namespace OnlineLearning_API.DTO.QuestionDTO
{
    public class ViewQuestionDTO
    {
        public Guid QuestionId { get; set; }
        public Guid TestId { get; set; }
        public Guid? ResourceId { get; set; }
        public int? QuestionOrder { get; set; }
        public string? Content { get; set; }
        public HashSet<ViewAnswerDTO> answers { get; set; }
    }
}
