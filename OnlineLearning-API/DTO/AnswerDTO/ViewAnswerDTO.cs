﻿namespace OnlineLearning_API.DTO.AnswerDTO
{
    public class ViewAnswerDTO
    {
        public Guid AnswerId { get; set; }
        public Guid QuestionId { get; set; }
        public int? AnswerOrder { get; set; }
        public string? Content { get; set; }
        public bool? IsCorrect { get; set; }
    }
}
