﻿namespace OnlineLearning_API.DTO.AnswerDTO
{
    public class AddAnswerDTO
    {
        //public Guid QuestionId { get; set; }
        public int? AnswerOrder { get; set; }
        public string? Content { get; set; }
        public bool? IsCorrect { get; set; }
    }
}
