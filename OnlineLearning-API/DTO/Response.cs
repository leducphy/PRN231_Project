﻿namespace OnlineLearning_API.DTO
{
    public class LoginResponse
    {
        public Guid Id { get; set; }
        public string FullName { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Role { get; set; } = null!;
        public string Token { get; set; } = null!;
    }

    public class RefreshToken
    {
        public string Token { get; set; } = string.Empty;
        public DateTime Created { get; set; } = DateTime.Now;
        public DateTime Expires { get; set; }
    }

    public class CurriculumnResponse
    {
        public Guid CurriculumId { get; set; }
        public string CurriculumTitle { get; set; } = string.Empty;
        public Guid ClassId { get; set; }
        public List<ChapterResponse> Chapters { get; set; }
    }

    public class ChapterResponse
    {
        public Guid ChapterId { get; set; }
        public string ChapterTitle { get; set; } = string.Empty;
        public List<MaterialResponse> Materials { get; set; }
    }

    public class MaterialResponse
    {
        public Guid MaterialId { get; set; }
        public Guid ResourceId { get; set; }
        public string MaterialTitle { get; set; }
    }

}
