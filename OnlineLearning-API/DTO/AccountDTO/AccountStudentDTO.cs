﻿namespace OnlineLearning_API.DTO.AccountDTO
{
    public class AccountStudentDTO
    {
        public Guid AccountId { get; set; }
        public string Name { get; set; } = null!;
        public string? ProfilePicture { get; set; }
        public string? School { get; set; }

    }
}
