﻿namespace OnlineLearning_API.DTO
{
    public class ClassInfoDTO
    {

        public Guid ClassId { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }
        public string? ClassPicture { get; set; }

        public int? testCount { get; set; }

        public int? studentCount { get; set; }  

      
    }
}

