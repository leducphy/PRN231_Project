﻿using AutoMapper;
using BusinessObject.Utility;
using BussinessObject.Models;
using DataAccess.Repository;
using DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineLearning_API.DTO;
using System.Threading;

namespace OnlineLearning_API.Controllers
{
    [Route("api/Curriculum")]
    [ApiController]
    public class CurriculumnController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICurriculumnRepository _curriculumnRepository;

        public CurriculumnController(
            IMapper mapper,
            ICurriculumnRepository curriculumnRepository,
            IConfiguration configuration)
        {
            _mapper = mapper;
            _configuration = configuration;
            _curriculumnRepository = curriculumnRepository;
        }

        [HttpGet("GetByID"), Authorize(Roles = RoleConstant.TEACHER + ", " + RoleConstant.STUDENT)]
        public async Task<ActionResult<IEnumerable<CurriculumnResponse>>> GetCurrilumn(Guid id)
        {
            var res = await _curriculumnRepository.ViewDetails(id);
            var curriculumResponse = _mapper.Map<CurriculumnResponse>(res);
            return Ok(curriculumResponse);
        }

        [HttpPost("Create"), Authorize(Roles = RoleConstant.TEACHER)]
        public async Task<IActionResult> Create([FromBody] CreateCurriculumnRequest newCurriculum)
        {
            var mapCur = _mapper.Map<Curriculum>(newCurriculum);
            var success = await _curriculumnRepository.AddCurriculumn(mapCur);

            if (success)
                return Ok("Curriculum created successfully");
            else
                return BadRequest("Failed to create curriculum");
        }
    }
}