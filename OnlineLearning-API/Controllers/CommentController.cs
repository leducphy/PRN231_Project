﻿using AutoMapper;
using BussinessObject.Models;
using DataAccess.Repository;
using DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Mvc;
using OnlineLearning_API.Exceptions;

namespace OnlineLearning_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : Controller
    {
        private readonly online_learningContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICommentRepository _commentRepository;

        public CommentController(online_learningContext context, IMapper mapper, ICommentRepository commentRepository, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
            _commentRepository = commentRepository;

        }
        [HttpPost]
        public async Task<ActionResult> CreateComment([FromForm] string? postId, [FromForm] string? accountId, [FromForm] string? content)
        {
            if (string.IsNullOrEmpty(postId) || string.IsNullOrEmpty(accountId) || string.IsNullOrEmpty(content))
            {
                throw new NotFoundException("Hãy kiểm tra lại dữ liệu đầu vào", 400);
            }

            var res = await _commentRepository.CreateComment(postId, accountId, content);

            return Ok(res);
        }
    }
}
