﻿using AutoMapper;
using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineLearning_API.DTO.AnswerDTO;

namespace OnlineLearning_API.Controllers
{
    [Route("api/Answer")]
    [ApiController]
    public class AnswerController : ControllerBase
    {
        private IMapper _mapper;
        private IAnswerRepository _answerRepository;

        public AnswerController(IMapper mapper, IAnswerRepository answerRepository)
        {
            _mapper = mapper;
            _answerRepository = answerRepository;
        }

        [HttpPost("AddAnswers")]
        public async Task<IActionResult> AddAnswers(List<AddAnswerDTO> answers)
        {
            try
            {
                var viewAnswers = await _answerRepository.AddAnswers(_mapper.Map<List<Answer>>(answers));
                return Ok(_mapper.Map<List<ViewAnswerDTO>>(viewAnswers));
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAnswers([FromQuery] Guid QuestionId)
        {
            var answers = await _answerRepository.GetAnswers(QuestionId);
            if (answers != null)
            {
                return Ok(_mapper.Map<List<ViewAnswerDTO>>(answers));
            }
            return NotFound();
        }
    }
}
