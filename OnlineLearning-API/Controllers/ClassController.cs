﻿using AutoMapper;
using BusinessObject.Utility;
using BussinessObject.Models;
using DataAccess.Repository;
using DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineLearning_API.DTO;
using OnlineLearning_API.DTO.AccountDTO;
using OnlineLearning_API.DTO.CommentDTO;
using OnlineLearning_API.DTO.PostDTO;
using OnlineLearning_API.DTO.ResourceDTO;
using OnlineLearning_API.Exceptions;
using System.Security.Claims;

namespace OnlineLearning_API.Controllers
{
    [Route("api/Class")]
    [ApiController]
    public class ClassController : Controller
    {
        private readonly online_learningContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IClassRepository _classRepository;

        public ClassController(online_learningContext context,IMapper mapper, IClassRepository classRepository, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
            _classRepository = classRepository;

        }
        
        [HttpGet("JoinClass")]
        public async Task<ActionResult<JoinClassResponse>> JoinClassByCode([FromQuery] string? classCode, [FromQuery] string? accountId)
        {
            CheckExistCodeAndAccountId(classCode, accountId);

            var statusChecked = await _classRepository.CheckJoinClass(classCode, accountId);
            string message = MessageCheck(statusChecked);

            JoinClassResponse response = new JoinClassResponse
            {
                statusChecked = statusChecked,
                Message = message
            };

            return response;
        }

        [HttpDelete("LeaveClass")]
        public async Task<ActionResult<bool>> LeaveClass([FromQuery] string? classCode, [FromQuery] string? accountId)
        {
            CheckExistCodeAndAccountId(classCode, accountId);
            var isLeave = await _classRepository.LeaveClass(classCode, accountId);

            return isLeave;
        }

        
        private static string MessageCheck(int statusChecked)
        {
            string message = "";
            switch (statusChecked)
            {
                case 1:
                    message = "Mã lớp học không tồn tại. Xin vui lòng thử lại";
                    break;
                case 2:
                    message = "Bạn đã tham gia lớp học này. Xin vui lòng nhập mã lớp học khác.";
                    break;
                case 3:
                    message = "Bạn đã tham gia lớp thành công. Hãy đợi giáo viên xác thực yêu cầu của bạn.";
                    break;
                case 4:
                    message = "Bạn đã tham gia lớp thành công. Enjoy !.";
                    break;
                default:
                    break;
            }

            return message;
        }

        private static void CheckExistCodeAndAccountId(string? classCode, string? accountId)
        {
            if (string.IsNullOrEmpty(classCode) || classCode.Length < 5)
            {
                throw new NotFoundException("Mã lớp học không hợp lệ", 400);
            }

            if (string.IsNullOrEmpty(accountId))
            {
                throw new NotFoundException("Bạn hãy đăng nhập lại chương trình", 400);
            }
        }

        public class JoinClassResponse
        {
            public int? statusChecked { get; set; }
            public string? Message { get; set; }
        }

        [HttpPost("CreateClass")]
        public IActionResult AddClass([FromBody] CreateClassDTO classDto)
        {
            try
            {
                _classRepository.AddClass(classDto.Name, classDto.EnrollApprove, classDto.ClassPicture , classDto.AccountId);
                return Ok("Tạo lớp học thành công ");
            }
            catch (Exception ex)
            {
                return BadRequest($"Lỗi khi tạo lớp học : {ex.InnerException?.Message ?? ex.Message}");
            }
        }


        
        [HttpGet("ListClass")]
      
        public IActionResult GetClassesByAccountId(Guid accountId)
        {
       
            var classes = _classRepository.GetClassesByAccountId(accountId);
            var classInfos = new List<ClassInfoDTO>() ;
            foreach (var item in classes) {
                var classDTO = _mapper.Map<ClassInfoDTO>(item);
                classDTO.testCount = item.Tests.Count();
                classDTO.studentCount = _classRepository.GetStudentsByClassId(item.ClassId , true).Count();
                classInfos.Add(classDTO);
            }
            return Ok(classInfos);
        }

        [HttpGet("ListClassStudent")]

        public IActionResult GetClassesByAccountIdStudent(Guid accountId)
        {

            var classes = _classRepository.GetClassesByAccountIdStudent(accountId);
            var classInfos = new List<ClassInfoDTO>();
            foreach (var item in classes)
            {
                var classDTO = _mapper.Map<ClassInfoDTO>(item);
                classDTO.testCount = item.Tests.Count();
                classDTO.studentCount = _context.Accounts.Where(a => a.Enrollments.Any(e => e.ClassId == item.ClassId)).Count();
                classInfos.Add(classDTO);
            }
            return Ok(classInfos);
        }



        [HttpGet("ListStudent")]

        public IActionResult GetStudentsByClassId(Guid classId , bool Accepted)
        {

            var students = _classRepository.GetStudentsByClassId(classId , Accepted);
            var listStudentDTO = new List<AccountStudentDTO>();
            foreach (var item in students)
            {
                var studentDTO = _mapper.Map<AccountStudentDTO>(item);
                listStudentDTO.Add(studentDTO);
            }
            return Ok(listStudentDTO); 
        }

        [HttpGet("ListApprove")]

        public IActionResult GetStudentsApprove(Guid classId, bool Accepted)
        {

            var students = _classRepository.GetStudentsByClassId(classId, Accepted);
            var listApproveDTO = new List<AccountApproveDTO>();
            foreach (var item in students)
            {
                var studentDTO = _mapper.Map<AccountApproveDTO>(item);
                listApproveDTO.Add(studentDTO);
            }
            return Ok(listApproveDTO);
        }

        [HttpPut("AcceptStudent")]
        public void AcceptStudent(Guid classId , List<Guid> AccountId) { 
            _classRepository.AcceptStudent(classId, AccountId);
        }


        [HttpPut("DeleteClass")]
        public void DeleteClass(Guid classId)
        {
            _classRepository.DeleteClass(classId);
        }

        [HttpGet("CountClass")]

        public IActionResult CountClass()
        {
            int count = _classRepository.CountClass();  
            return Ok(count);   
        }

        [HttpGet("TotalAccount")]

        public IActionResult TotalStudent(int role)
        {
            int count = _classRepository.CountAccountByRole(role);
            return Ok(count);
        }

        [HttpGet("TopClass")]

        public IActionResult TopClass()
        {

            var classes = _classRepository.GetTop3Class();
           
            return Ok(classes);
        }






    }
}
