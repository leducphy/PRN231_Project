﻿using AutoMapper;
using BusinessObject.Utility;
using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using OnlineLearning_API.Configurations;
using OnlineLearning_API.DTO;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mail;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Security.Principal;
using System.Security.Cryptography;

namespace OnlineLearning_API.Controllers
{
    [Route("api/Auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly online_learningContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IAccountRepository _accountRepository;

        public AuthController(
            online_learningContext context,
            IMapper mapper, IAccountRepository
            accountRepository,
            IConfiguration configuration
            )
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
            _accountRepository = accountRepository;
        }


        [HttpGet("GetAll"), Authorize(Roles = RoleConstant.ADMIN)]

        public async Task<ActionResult<IEnumerable<Account>>> GetAllAccount()
        {
            return await _context.Accounts.ToListAsync();
        }

        [HttpGet]
        [Route("Verify")]
        public async Task<IActionResult> Verify(Guid token)
        {
            try
            {
                bool isVerified = await _accountRepository.Verify(token);

                if (isVerified)
                {
                    return Ok("Verification successful");
                }
                else
                {
                    return BadRequest("Invalid token");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error {ex}");
            }
        }

        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordRequest rq)
        {
            try
            {
                if (await _accountRepository.CheckAddExistEmail(rq.Email)) { return StatusCode(500, "Email is not existed !"); }
                var reserToken = await _accountRepository.ForgotPassword(rq.Email);
                string mailSubject = "[LE.ON] Password Reset";
                string body = $"\r\nTrouble signing in?\r\n\r\nResetting your password is easy.\r\n\r\nJust press the button below and follow the instructions. We’ll have you up and running in no time.\r\nhttp://localhost:8888/Auth/ResetPassword?token={reserToken}\r\n\r\nIf you did not make this request then please ignore this email.\r\n";
                SendMail(rq.Email, mailSubject, body);
                return Ok(reserToken);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordRequest rq)
        {
            try
            {
                var newPass = PasswordHasher.HashPassword(rq.ConfirmPassword);
                bool status = await _accountRepository.ResetPassword(rq.Token, newPass);
                if (status)
                {
                    return Ok("Password reset successful");
                }
                else
                {
                    return BadRequest("Invalid token or password reset failed");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("SignUp")]
        public async Task<IActionResult> Add([FromBody] AccountAddRequest rq)
        {
            try
            {
                string pass = PasswordHasher.HashPassword(rq.Password);
                if (!await _accountRepository.CheckAddExistEmail(rq.Email)) { return BadRequest("Email is already existed !"); }
                rq.VerificationCode = Guid.NewGuid();
                rq.CreateTime = DateTime.Now;
                rq.Password = pass;
                rq.Locked = false;
                var map = _mapper.Map<Account>(rq);
                string mailSubject = "[LE.ON] Verify your account";
                string body = $"\r\nHello {rq.Name}\r\n\r\nYou registered an account on Online Learning, before being able to use your account you need to verify that this is your email address by clicking here: http://localhost:8888/Auth/Verify?token={rq.VerificationCode}\r\n\r\nKind Regards, Le.On";
                SendMail(rq.Email, mailSubject, body);
                var rs = await _accountRepository.Add(map);

                return Ok($"account created, please check your email to active your account, active code : {rq.VerificationCode}");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("SignIn")]
        public async Task<ActionResult<LoginResponse>> Login(LoginRequestModel model)
        {
            try
            {
                var passwordHashed = PasswordHasher.HashPassword(model.Password);


                var account = await _accountRepository.Login(model.Email, passwordHashed);
                var roleName = GetRoleName(account.Role);

                if (await _accountRepository.CheckAddExistEmail(model.Email)) { return StatusCode(500, "Email is not existed !"); }
                if (account == null) return BadRequest(new { error = "Invalid email or password" });
                if (account.Locked == true) return BadRequest("Account is locked");
                if (account.VerifiedAt == null) return BadRequest("Account is not activate yet");

                var token = CreateToken(account);

                var loginRes = new LoginResponse
                {
                    Id = account.AccountId,
                    Email = account.Email,
                    FullName = account.Name,
                    Role = roleName,
                    Token = token
                };

                return Ok(loginRes);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //[HttpPost("RefreshToken")]
        //public async Task<ActionResult<string>> RefreshToken()
        //{
        //    var refreshToken = Request.Cookies["refreshToken"];

        //    if (!user.RefreshToken.Equals(refreshToken))
        //    {
        //        return Unauthorized("Invalid Refresh Token.");
        //    }
        //    else if (user.TokenExpires < DateTime.Now)
        //    {
        //        return Unauthorized("Token expired.");
        //    }

        //    string token = CreateToken(user);
        //    var newRefreshToken = GenerateRefreshToken();
        //    SetRefreshToken(newRefreshToken);

        //    return Ok(token);
        //}

        //private RefreshToken GenerateRefreshToken()
        //{
        //    var refreshToken = new RefreshToken
        //    {
        //        Token = Convert.ToBase64String(RandomNumberGenerator.GetBytes(64)),
        //        Expires = DateTime.Now.AddDays(7),
        //        Created = DateTime.Now
        //    };

        //    return refreshToken;
        //}

        //private void SetRefreshToken(RefreshToken newRefreshToken)
        //{
        //    var cookieOptions = new CookieOptions
        //    {
        //        HttpOnly = true,
        //        Expires = newRefreshToken.Expires
        //    };
        //    Response.Cookies.Append("refreshToken", newRefreshToken.Token, cookieOptions);

        //    user.RefreshToken = newRefreshToken.Token;
        //    user.TokenCreated = newRefreshToken.Created;
        //    user.TokenExpires = newRefreshToken.Expires;
        //}


        private string CreateToken(Account account)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("JwtKey"));
            var roleName = GetRoleName(account.Role);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _configuration["JwtIssuer"],
                Subject = new ClaimsIdentity(new Claim[]
                {
            new Claim(ClaimTypes.Sid, account.AccountId.ToString()),
            new Claim(ClaimTypes.Name, $"{account.Name}"),
            new Claim(ClaimTypes.Email, account.Email),
            new Claim(ClaimTypes.Role, roleName),
                }),
                Expires = DateTime.Now.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenWrite = tokenHandler.WriteToken(token);

            return tokenWrite;
        }

        public static string GetRoleName(int roleNumber)
        {
            switch (roleNumber)
            {
                case 0:
                    return "Admin";
                case 1:
                    return "Teacher";
                case 2:
                    return "Student";
                default:
                    return "Unknown Role";
            }
        }


        private void SendMail(string email, string sub, string body)
        {
            try
            {
                // Cấu hình SmtpClient
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential("onlinelearning.manage.service@gmail.com", "fulm hocl ocyg kfmw");
                smtpClient.EnableSsl = true;

                // Tạo đối tượng MailMessage
                MailMessage message = new MailMessage();
                message.From = new MailAddress("onlinelearning.manage.service@gmail.com");
                message.To.Add(email);
                message.Subject = sub;
                message.Body = body;
                // Gửi email
                smtpClient.Send(message);

                Console.WriteLine("Password reset email sent successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error sending password reset email: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAccount/{id}")]
        public async Task<ActionResult<Account>> GetAccount(Guid id)
        {
            return await _accountRepository.GetAccountById(id);
        }

        [HttpPut]
        [Route("Edit/{id}")]
        public async Task<ActionResult<Account>> EditAccount(Guid id, EditProfileRequest? editProfileRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Account account = await _accountRepository.GetAccountById(id);
            if (account == null)
            {
                return NotFound();
            }
            if (!string.IsNullOrEmpty(editProfileRequest?.Name))
            {
                account.Name = editProfileRequest.Name;
            }
            else if (editProfileRequest?.Dob != null)
            {
                account.Dob = editProfileRequest.Dob;
            }
            else if (editProfileRequest?.Gender != null)
            {
                account.Gender = editProfileRequest.Gender;
            }
            else if (!string.IsNullOrEmpty(editProfileRequest?.Phone))
            {
                account.Phone = editProfileRequest.Phone;
            }
            else if (!string.IsNullOrEmpty(editProfileRequest?.Address))
            {
                account.Address = editProfileRequest.Address;
            }
            else if (!string.IsNullOrEmpty(editProfileRequest?.School))
            {
                account.School = editProfileRequest.School;
            }
            var rs = await _accountRepository.Update(account);
            if (rs == true) return Ok(account);
            else return BadRequest(account);
        }
    }
}
