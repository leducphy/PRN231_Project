﻿using AutoMapper;
using BussinessObject.Models;
using DataAccess.Repository;
using DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineLearning_API.DTO.CommentDTO;
using OnlineLearning_API.DTO.PostDTO;
using OnlineLearning_API.DTO.ResourceDTO;
using OnlineLearning_API.Exceptions;

namespace OnlineLearning_API.Controllers
{
    [Route("api/Post")]
    [ApiController]
    public class PostController : Controller
    {
        private readonly online_learningContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IPostRepository _postRepository;

        public PostController(online_learningContext context, IMapper mapper, IPostRepository postRepository, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
            _postRepository = postRepository;

        }
        [HttpPost]
        public async Task<ActionResult<Guid>> CreatePost([FromForm] string? classId, [FromForm] string? accountId, [FromForm] string? content, [FromForm] List<IFormFile> listBase64)
        {
            CheckExistCodeAndAccountId(classId, accountId);

            // Convert files to Base64 strings
            List<(string base64String, string mimeType)> base64ListWithMimeType = await ConvertToBase64WithMimeType(listBase64);

            var postId = await _postRepository.CreatePost(classId, accountId, content, base64ListWithMimeType);

            return postId;
        }

        [HttpGet("content")]
        public async Task<ActionResult> GetClassByCode(string? classId)
        {
            if (string.IsNullOrEmpty(classId))
            {
                throw new NotFoundException("Id lớp học không hợp lệ", 400);
            }

            var respon = await _context.Posts.Include(z => z.Resources).Include(g => g.Comments)
                .Where(x => x.ClassId == Guid.Parse(classId)).OrderByDescending(x => x.CreateTime)
                .ToListAsync();

            var res = respon.Select(x => new PostResourceDTO()
            {
                PostId = x.PostId,
                Content = x.Content,
                AccountName = _context.Accounts.FirstOrDefault(o => o.AccountId == x.AccountId).Name,
                CreateTime = x.CreateTime,
                ResourcePostDTO = x.Resources.Select(y => new ResourcePostDTO()
                {
                    Url = y.Url,
                    MimeType = y.MimeType
                }).ToList(),
                CommentPostDTO = x.Comments.Select(z => new CommentPostDTO()
                {
                    Content = z.Content,
                    AccountName = _context.Accounts.FirstOrDefault(x => x.AccountId == z.AccountId).Name
                }).ToList()
            }).ToList(); // Materialize the outer collection by calling ToList()

            return Ok(res);
        }


        private static async Task<List<(string base64String, string mimeType)>> ConvertToBase64WithMimeType(List<IFormFile> listBase64)
        {
            var base64ListWithMimeType = new List<(string base64String, string mimeType)>();
            foreach (var file in listBase64)
            {
                using (var ms = new MemoryStream())
                {
                    await file.CopyToAsync(ms);
                    var fileBytes = ms.ToArray();
                    var base64String = Convert.ToBase64String(fileBytes);
                    var mimeType = file.ContentType;
                    base64ListWithMimeType.Add((base64String, mimeType));
                }
            }

            return base64ListWithMimeType;
        }

        private static async Task<List<string>> ConvertToBase64(List<IFormFile> listBase64)
        {
            var base64List = new List<string>();
            foreach (var file in listBase64)
            {
                using (var ms = new MemoryStream())
                {
                    await file.CopyToAsync(ms);
                    var fileBytes = ms.ToArray();
                    var base64String = Convert.ToBase64String(fileBytes);
                    base64List.Add(base64String);
                }
            }
            return base64List;
        }


        private static void CheckExistCodeAndAccountId(string? classId, string? accountId)
        {
            if (string.IsNullOrEmpty(classId))
            {
                throw new NotFoundException("Id lớp học không hợp lệ", 400);
            }

            if (string.IsNullOrEmpty(accountId))
            {
                throw new NotFoundException("Bạn hãy đăng nhập lại chương trình", 400);
            }
        }
    }
}
