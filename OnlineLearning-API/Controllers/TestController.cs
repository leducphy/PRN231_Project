﻿using AutoMapper;
using BussinessObject.Models;
using DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineLearning_API.DTO.TestDTO;
using OnlineLearning_API.Exceptions;

namespace OnlineLearning_API.Controllers
{
    [Route("api/Test")]
    [ApiController]
    public class TestController : ControllerBase
    {
        ITestReposity _testRepository;
        IMapper _mapper;
        IAccountRepository _accountRepository;
        public TestController(ITestReposity testRepository, IMapper mapper, IAccountRepository accountRepository)
        {
            _testRepository = testRepository;
            _mapper = mapper;
            _accountRepository = accountRepository;
        }
        [HttpPost("CreateTest")]
        public async Task<IActionResult> AddTest([FromBody] AddTestDTO test)
        {
            if (test == null)
            {
                return BadRequest(new InternalException("Invalid test"));
            }
            test.StartAt = test.StartAt == null ? DateTime.Now : test.StartAt;
            test.CreateTime = DateTime.Now;
            var result = await _testRepository.addTest(_mapper.Map<Test>(test));
            return Ok(_mapper.Map<ViewTestDTO>(result));
        }

        [HttpGet("{testId}")]
        public async Task<IActionResult> getTest([FromRoute] Guid testId)
        {
            var test = _mapper.Map<ViewTestDTO>(_testRepository.GetTest(testId));
            if (test != null)
            {
                return Ok(test);
            }
            return NotFound();
        }

        [HttpGet("GetTestList")]
        public async Task<IActionResult> getTests([FromQuery] Guid? classId)
        {            
            if (classId != null)
            {
                var result = _testRepository.getTests(classId);
                return Ok(_mapper.Map<List<ViewTestDTO>>(result));
            }
            return Ok(_mapper.Map<List<ViewTestDTO>>(_testRepository.getTests()));
        }

        [HttpGet("Transcript/{cid}")]
        public async Task<IActionResult> GetTranscript(Guid cid)
        {
            var accounts = await _accountRepository.GetAccountsByClassId(cid);
            return Ok(accounts);
        }

        [HttpGet("GetTestList/{cid}")]
        public async Task<ActionResult<IEnumerable<Test>>> getTests(Guid cid)
        {
            return Ok(_testRepository.getTestsByClassId(cid));
        }

        [HttpPut("UpdateTest")]
        public async Task<IActionResult> updateTest([FromBody] ViewTestDTO test)
        {
            Test res = _testRepository.UpdateTest(_mapper.Map<Test>(test));
            return Ok(_mapper.Map<ViewTestDTO>(res));
        }
    }
}
