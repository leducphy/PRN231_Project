﻿//using AutoMapper;
//using BussinessObject.Models;
//using DataAccess.Repository.IRepository;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using OnlineLearning_API.DTO.QuestionDTO;

//namespace OnlineLearning_API.Controllers
//{
//    [Route("api/Question")]
//    [ApiController]
//    public class QuestionController : ControllerBase
//    {
//        private IQuestionReposity _questionRepository;
//        private IMapper _mapper;
//        public QuestionController(IQuestionReposity questionReposity, IMapper mapper)
//        {
//            _questionRepository = questionReposity;
//            _mapper = mapper;
//        }
//        [HttpPost("/AddQuestions")]
//        public async Task<IActionResult> AddQuestions(List<AddQuestionDTO> questions)
//        {
//            try
//            {
//                var questionsSet = await _questionRepository.AddQuestions(_mapper.Map<List<Question>>(questions));
//                return Ok(_mapper.Map<List<ViewQuestionDTO>>(_mapper.Map<List<Question>>(questionsSet)));
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, $"Internal server error: {ex.Message}");
//            }
//        }

//        [HttpGet("/{TestId}")]
//        public async Task<IActionResult> GetQuestions([FromRoute] Guid TestId)
//        {
//            return Ok(_mapper.Map<IEnumerable<ViewQuestionDTO>>(_questionRepository.GetQuestions(TestId)));
//        }

//        [HttpGet("/{QuestionId}")]
//        public async Task<IActionResult> GetQuestion([FromRoute] Guid QuestionId)
//        {
//            var q = _questionRepository.GetQuestion(QuestionId);
//            if (q != null)
//            {
//                return Ok(_mapper.Map<ViewQuestionDTO>(q));
//            }
//            return NotFound();
//        }
//    }
//}
