﻿using Microsoft.AspNetCore.Mvc;

namespace OnlineLearning_API.Exceptions
{
    public class InternalException : Exception
    {
        #region Constructor
        /// <summary>
        /// Khởi tạo constructor không tham số
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public InternalException() { }
        /// <summary>
        /// Khởi tạo constructor có tham số
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public InternalException(string mess) : base(mess) { }
        #endregion
    }
}
