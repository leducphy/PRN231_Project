﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace OnlineLearning_API.Exceptions
{
    public class BaseException
    {
        #region Properties
        /// <summary>
        /// Mã lỗi
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Thông báo dành cho dev
        /// </summary>
        public string? DevMsg { get; set; }

        /// <summary>
        /// Thông báo dành cho users
        /// </summary>
        public string? UserMsg { get; set; }

        /// <summary>
        /// Id trace
        /// </summary>
        public string? TraceId { get; set; }

        /// <summary>
        /// Các thông tin thêm
        /// </summary>
        public string? MoreInfo { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Chuyển đối tượng hiện tại (mã hóa) sang chuỗi JSON bằng cách sử dụng phương thức Serialize
        /// </summary>
        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
        #endregion
    }
}
