﻿using Microsoft.AspNetCore.Mvc;

namespace OnlineLearning_API.Exceptions
{
    public class NotFoundException : Exception
    {
        #region Properties
        /// <summary>
        /// Mã lỗi
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public int ErrorCode { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Khởi tạo constructor không tham số
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public NotFoundException() { }
        /// <summary>
        /// Khởi tạo constructor có tham số
        /// </summary>
        /// <param name="errorCode">mã lỗi</param>
        ///  Author: NDANH (23/05/2023)
        public NotFoundException(int errorCode)
        {
            ErrorCode = errorCode;
        }
        /// <summary>
        /// Khởi tạo constructor có tham số
        /// </summary>
        /// <param name="mess">thông báo lỗi</param>
        ///  Author: NDANH (23/05/2023)
        public NotFoundException(string mess) : base(mess)
        {
        }
        /// <summary>
        /// Khởi tạo constructor có tham số
        /// </summary>
        /// <param name="errorCode">mã lỗi</param>
        /// <param name="mess">thông báo lỗi</param>
        ///  Author: NDANH (23/05/2023)
        public NotFoundException(string mess, int errorCode) : base(mess)
        {
            ErrorCode = errorCode;
        }
        #endregion
    }
}
