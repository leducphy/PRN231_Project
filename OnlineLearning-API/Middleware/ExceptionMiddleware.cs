﻿using Microsoft.AspNetCore.Mvc;
using OnlineLearning_API.Exceptions;
using System.Net;
using System.Runtime.InteropServices;

namespace OnlineLearning_API.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Khởi tạo đối tượng middleware ExceptionMiddleware
        /// lưu trữ tham chiếu đến middleware tiếp theo trong pipeline để sử dụng
        /// sau đó để chuyển yêu cầu đến middleware tiếp theo.
        /// </summary>
        /// Author: NDANH (26/05/2023)
        /// <param name="next"> tham số kiểu RequestDelegate</param>
        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Khi có yêu cầu HTTP đi qua middleware,
        /// sẽ thực hiện gọi phương thức tiếp theo (_next) trong pipeline 
        /// và bắt exception nếu có.
        /// </summary>
        /// Author: NDANH (26/05/2023)
        /// <param name="context">Các thông tin về yêu cầu web hiện tại</param>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }
        /// <summary>
        /// Phân loại và xử lý chính xác các exception bắt được
        /// </summary>
        /// Author: NDANH (26/05/2023)
        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";

            if (exception is NotFoundException)
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                await context.Response.WriteAsync(
                    text: new BaseException()
                    {
                        ErrorCode = context.Response.StatusCode,
                        //UserMsg = MISAResource.UserMsg_Exception,
                        UserMsg = "Loi",
                        DevMsg = exception.Message,
                        TraceId = context.TraceIdentifier,
                        MoreInfo = exception.HelpLink
                    }.ToString() ?? ""
                );
            }
            else if (exception is InternalException)
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                await context.Response.WriteAsync(
                    text: new BaseException()
                    {
                        ErrorCode = context.Response.StatusCode,
                        UserMsg = "Loi",
                        DevMsg = exception.Message,
                        TraceId = context.TraceIdentifier,
                        MoreInfo = exception.HelpLink
                    }.ToString() ?? ""
                );
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                await context.Response.WriteAsync(
                    text: new BaseException()
                    {
                        ErrorCode = context.Response.StatusCode,
                        UserMsg = "Loi",
                        DevMsg = exception.Message,
                        TraceId = context.TraceIdentifier,
                        MoreInfo = exception.HelpLink
                    }.ToString() ?? ""
                );
            }

        }
    }
}
