using AutoMapper;
using BusinessObject.Utility;
using BussinessObject.Models;
using OnlineLearning_API.DTO;
using OnlineLearning_API.DTO.AccountDTO;
using OnlineLearning_API.DTO.AnswerDTO;
using OnlineLearning_API.DTO.QuestionDTO;
using OnlineLearning_API.DTO.TestDTO;
using System.Threading.Channels;

namespace OnlineLearning_API.Configurations.Mappers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Account, Account>().ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));
            CreateMap<Account, AccountAddRequest>().ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));
            CreateMap<Account, EditProfileRequest>().ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));
            CreateMap<Class, ClassInfoDTO>();

            CreateMap<Class,ClassInfoDTO>();
            CreateMap<Account , AccountStudentDTO>().ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));
            CreateMap<Account, AccountApproveDTO>().ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));
            CreateMap<AddQuestionDTO, Question>();
            CreateMap<AddAnswerDTO, Answer>();
            CreateMap<AddTestDTO, Test>();

            CreateMap<Answer, ViewAnswerDTO>();
            CreateMap<Question, ViewQuestionDTO>();
            CreateMap<Test, ViewTestDTO>();


            CreateMap<ViewAnswerDTO, Answer>();
            CreateMap<ViewQuestionDTO, Question>();
            CreateMap<ViewTestDTO, Test>();

            CreateMap<Curriculum, CreateCurriculumnRequest>()
            .ForMember(dest => dest.ClassId, opt => opt.MapFrom(src => src.ClassId))
            .ForMember(dest => dest.Chapters, opt => opt.MapFrom(src => src.Chapters)).ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));
            CreateMap<Curriculum, CurriculumnResponse>().ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null)); 

            CreateMap<Chapter, ChapterAddRequest>()
                .ForMember(dest => dest.Materials, opt => opt.MapFrom(src => src.Materials)).ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));
            CreateMap<Chapter, ChapterResponse>().ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));

            CreateMap<Material, MaterialAddRequest>().ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));
            CreateMap<Material, MaterialResponse>().ReverseMap().ForAllMembers(x => x.Condition((source, target, sourceValue) => sourceValue != null));

        }

    }
}
